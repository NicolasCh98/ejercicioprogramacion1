<?php
session_start();
function convertirAMinusculas($cadena) {
    date_default_timezone_set('America/New_York');
    $cadena_en_minusculas = strtolower($cadena);
    return $cadena_en_minusculas;
}

function registrarOperacion($mensaje) {
    $registro = date("Y-m-d H:i:s") . " - " . $mensaje . PHP_EOL;
    error_log($registro, 3, "operaciones.log");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    try {
        $username = $_POST['username'];
        $password = $_POST['password'];

        // Conectar a la base de datos usando PDO
        $pdo = new PDO("pgsql:host=localhost;dbname=login", "postgres", "postgres");
        registrarOperacion("Intento de inicio de sesión para el usuario: $username");

        

        // Consulta para verificar las credenciales
        $query = $pdo->prepare("SELECT id, nombre, apellido, password FROM users WHERE nombre_usuario = :username");
        $query->execute(array(':username' => $username));

        $user = $query->fetch(PDO::FETCH_ASSOC);

        if ($user && hash('sha256', $password) === convertirAMinusculas($user['password'])) {
            // La contraseña es válida
            $_SESSION['nombre'] = $user['nombre'];
            $_SESSION['apellido'] = $user['apellido'];
            header("Location: welcome.php");
            exit();
        } else {
            // La contraseña no coincide
            // $debug = convertirAMinusculas($user['password']);
            // $debug2 = hash('sha256', $password);
            $error = "Usuario o contraseña incorrectos";
            registrarOperacion("Inicio de sesión fallido para el usuario: $username");
        }

    } catch (PDOException $e) {
        // Manejar excepciones
        error_log("Error en la base de datos: " . $e->getMessage(), 0);
        registrarOperacion("Error en la base de datos: " . $e->getMessage());
        // $error = "Error en la base de datos. Por favor, inténtelo de nuevo más tarde.";
        $error = $e;
    }
}

// Mostrar el formulario de inicio de sesión con un mensaje de error si es necesario
?>
<!DOCTYPE html>
<html>
<head>
    <title>Inicio de Sesión</title>
    <!-- <link rel="stylesheet" type="text/css" href="styles.css"> -->
    <style>
        .error-message {
            color: red;
            font-weight: bold;
            margin-top: 10px;
        }

    </style>
</head>
<body>
    <div class="login-container">
        <h2>Iniciar Sesión</h2>
        <form action="login.php" method="POST">
            <input type="text" name="username" placeholder="Usuario" required>
            <input type="password" name="password" placeholder="Contraseña" required>
            <button type="submit">Iniciar Sesión</button>
        </form>
        <?php if (isset($error)) {
            echo "<p class='error-message'>$error</p>";
        } ?>
    </div>
</body>
</html>
