<?php
function registrarOperacion($mensaje) {
    date_default_timezone_set('America/New_York');
    $registro = date("Y-m-d H:i:s") . " - " . $mensaje . PHP_EOL;
    error_log($registro, 3, "operaciones.log");
}
session_start();
$nombre = $_SESSION['nombre'];
registrarOperacion("$nombre cerro sesion.");
session_unset();
session_destroy();
header("Location: login.php");
exit();
?>
