<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi Página con PHP</title>
</head>

<body>

    <?php
// Crear variables
$nombreApellido = "Tu Nombre y Apellido";
$nacionalidad = "Tu Nacionalidad";

// Imprimir nombre y apellido en negrita y rojo
echo "<p style='color: red; font-weight: bold;'>$nombreApellido</p>";

// Imprimir nacionalidad subrayada
print "<p style='text-decoration: underline;'>$nacionalidad</p>";
?>

</body>

</html>