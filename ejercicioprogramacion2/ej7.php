<?php
// Función para generar números aleatorios pares entre 1 y 10,000
function generarNumeroAleatorioPar() {
    return rand(1, 5000) * 2; // Se multiplica por 2 para asegurar un número par
}

// Array para almacenar los números aleatorios pares
$numerosPares = array();

// Generar 900 números aleatorios pares
for ($i = 0; $i < 900; $i++) {
    $numero = generarNumeroAleatorioPar();
    $numerosPares[] = $numero;
}

// Imprimir los números aleatorios pares
foreach ($numerosPares as $numero) {
    echo $numero . " ";
}
?>
