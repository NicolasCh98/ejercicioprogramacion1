<?php
$dbhost = "localhost";
$dbname = "ejercicio4";
$dbport = "5433";
$dbuser = "postgres";
$dbpass = "123";

// Función para insertar 1000 registros aleatorios
function insertarRegistros($pdo) {
    for ($i = 1; $i <= 1000; $i++) {
        $nombre = "Registro " . $i;
        $descripcion = "Descripción del registro " . $i;

        $sql = "INSERT INTO tabla_prueba (nombre, descripcion) VALUES (:nombre, :descripcion)";
        $stmt = $pdo->prepare($sql);
        $stmt->bindParam(':nombre', $nombre);
        $stmt->bindParam(':descripcion', $descripcion);
        $stmt->execute();
    }
}

try {
    $pdo = new PDO("pgsql:host=$dbhost;port=$dbport;dbname=$dbname", $dbuser, $dbpass);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    if (isset($_POST['insertar'])) {
        // Insertar registros al hacer clic en el botón "Insertar"
        insertarRegistros($pdo);
        echo "Se han insertado 1000 registros en la tabla.";
    }

    $sql = "SELECT * FROM tabla_prueba";
    $stmt = $pdo->query($sql);

    echo "<form method='post'>";
    echo "<input type='submit' name='insertar' value='Insertar 1000 Registros'>";
    echo "</form>";

    echo "<table border='1'>";
    echo "<tr><th>ID</th><th>Nombre</th><th>Descripción</th></tr>";

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        echo "<tr><td>{$row['id']}</td><td>{$row['nombre']}</td><td>{$row['descripcion']}</td></tr>";
    }

    echo "</table>";
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
?>