<?php
require 'vendor/autoload.php'; 

$apiKey = 'k73TegXwojFuTfozlZPTvcIXNl7w1veIutOhWbeT';


$baseURL = "https://api.nasa.gov";

$endpoint = "/planetary/apod";
date_default_timezone_set('America/Asuncion');
$date = date('Y-m-d');
$params = [
    'api_key' => $apiKey,
    'date' => $date, 
];

// URL completa para la solicitud
$url = $baseURL . $endpoint . '?' . http_build_query($params);

// Inicializa el cliente Guzzle
$client = new GuzzleHttp\Client();

try {
    $response = $client->get($url);

    $data = json_decode($response->getBody());

    if ($data) {
        echo "<html>";
        echo "<head>";
        echo "<style>";
        echo "body { font-family: Arial, sans-serif; background-color: #f0f0f0; text-align: center; }";
        echo "h1 { color: #0056b3; }";
        echo "h2 { color: #333; }";
        echo "img { max-width: 100%; height: auto; }";
        echo "p { margin: 20px; }";
        echo "</style>";
        echo "</head>";
        echo "<body>";
        echo "<h1>Imagen del día de la NASA</h1>";
        echo "<h2>{$data->title}</h2>";
        echo "<img src='{$data->url}' alt='{$data->title}'>";
        echo "<p>{$data->explanation}</p>";
        echo "</body>";
        echo "</html>";
    } else {
        echo "Error al obtener la imagen del día.";
    }
} catch (GuzzleHttp\Exception\RequestException $e) {
    echo "Error en la solicitud: " . $e->getMessage();
}
