<!DOCTYPE html>
<html>
<head>
    <title>Verificador de Palíndromos</title>
</head>
<body>
    <h1>Verificador de Palíndromos</h1>
    <form method="post">
        <label for="cadena">Introduce una cadena:</label>
        <input type="text" name="cadena" id="cadena" required>
        <input type="submit" name="verificar" value="Verificar">
    </form>

    <?php
    function esPalindromo($cadena) {
        $cadena = str_replace(' ', '', $cadena); 
        $cadena = strtolower($cadena); 
        $cadenaInvertida = strrev($cadena); 

        return $cadena == $cadenaInvertida;
    }

    if(isset($_POST['verificar'])){
        $cadena = $_POST['cadena'];

        if (esPalindromo($cadena)) {
            echo "<p>$cadena es un palíndromo.</p>";
        } else {
            echo "<p>$cadena no es un palíndromo.</p>";
        }
    }
    ?>
</body>
</html>
