<?php
function generarArray() {
    $array = array();
    $letras = 'abcdefghijklmnopqrstuvwxyz';

    for ($i = 0; $i < 10; $i++) {
        $longitudClave = rand(5, 10);
        $clave = '';
        for ($j = 0; $j < $longitudClave; $j++) {
            $clave .= $letras[rand(0, strlen($letras) - 1)];
        }

        $valor = rand(1, 1000);

        $array[$clave] = $valor;
    }

    return $array;
}
?>
