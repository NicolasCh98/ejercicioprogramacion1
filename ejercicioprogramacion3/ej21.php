<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario de Datos</title>
</head>
<body>
    <h1>Formulario de Datos</h1>

    <form action="" method="post">
        <label for="texto">Ingrese su nickname:</label>
        <input type="text" id="texto" name="texto"><br><br>

        <label for="password">Ingrese su contraseña:</label>
        <input type="password" id="password" name="password"><br><br>

        <label for="select_simple">De sexo:</label>
        <select id="select_simple" name="select_simple">
            <option value="Hombre">Hombre</option>
            <option value="Mujer">Mujer</option>
            <option value="Prefiero no decirlo">Prefiero no decirlo</option>
        </select><br><br>

        <label for="select_multiple">Vive con:</label>
        <select id="select_multiple" name="select_multiple[]" multiple>
            <option value="Padre">Padre</option>
            <option value="Madre">Madre</option>
            <option value="Hermanos">Hermanos</option>
        </select><br><br>

        <label for="checkbox">Es alumno de la facultad CyT:</label>
        <input type="checkbox" id="checkbox" name="checkbox"><br><br>

        <label>Radio Buttons:</label><br>
        <input type="radio" id="opcion1" name="radio" value="Analisis de Sistemas">
        <label for="opcion1">Analisis de Sistemas</label><br>
        <input type="radio" id="opcion2" name="radio" value="Diseño">
        <label for="opcion2">Diseño</label><br>
        <input type="radio" id="opcion3" name="radio" value="Arquitectura">
        <label for="opcion3">Arquitectura</label><br><br>

        <label for="textarea">Comentario:</label><br>
        <textarea id="textarea" name="textarea" rows="4" cols="50"></textarea><br><br>

        <button type="submit">Enviar</button>
    </form>

    <?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        echo "<h2>Datos del Formulario:</h2>";
        echo "<p><strong>Campo de Texto:</strong> " . htmlspecialchars($_POST['texto']) . "</p>";
        echo "<p><strong>Campo de Contraseña:</strong> " . htmlspecialchars($_POST['password']) . "</p>";
        echo "<p><strong>Select Simple:</strong> " . htmlspecialchars($_POST['select_simple']) . "</p>";
        echo "<p><strong>Select Múltiple:</strong> " . implode(", ", $_POST['select_multiple']) . "</p>";
        echo "<p><strong>Checkbox:</strong> " . (isset($_POST['checkbox']) ? 'Sí' : 'No') . "</p>";
        echo "<p><strong>Radio Button:</strong> " . htmlspecialchars($_POST['radio']) . "</p>";
        echo "<p><strong>Text Area:</strong> " . htmlspecialchars($_POST['textarea']) . "</p>";
    }
    ?>
</body>
</html>
