<?php
function imprimirReversa($vector, $indice) {
    if ($indice >= 0) {
        echo $vector[$indice] . ", ";
        imprimirReversa($vector, $indice - 1); 
    }
}


$vector = array();
for ($i = 0; $i < 20; $i++) {
    $vector[] = rand(1, 10);
}


echo "Vector en orden original: " . implode(", ", $vector) . "<br>";
echo "Vector en orden inverso: ";
imprimirReversa($vector, 19);
?>
