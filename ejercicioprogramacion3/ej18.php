<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Contador de Visitas</title>
</head>
<body>
    <h1>Bienvenido a nuestra página</h1>

    <?php
    function aumentarContador() {
        $archivo = 'contador.txt';
        if (file_exists($archivo)) {
            $contador = (int) file_get_contents($archivo);
            $contador++;
            file_put_contents($archivo, $contador);
        } else {
            file_put_contents($archivo, 1);
            $contador = 1;
        }
        return $contador;
    }

    $visitas = aumentarContador();
    ?>

    <p>Esta página ha sido visitada <?php echo $visitas; ?> veces.</p>
</body>
</html>
