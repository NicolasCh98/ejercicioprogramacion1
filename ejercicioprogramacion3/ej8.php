<!DOCTYPE html>
<html>
<head>
    <title>Generador de Matrices</title>
</head>
<body>
    <h1>Generador de Matrices</h1>
    <form method="post">
        <label for="filas">Número de filas:</label>
        <input type="number" name="filas" id="filas" required><br>
        <label for="columnas">Número de columnas:</label>
        <input type="number" name="columnas" id="columnas" required><br>
        <input type="submit" name="generar" value="Generar Matriz">
    </form>

    <?php
    function generarMatriz($filas, $columnas) {
        $matriz = array();
        for ($i = 0; $i < $filas; $i++) {
            for ($j = 0; $j < $columnas; $j++) {
                $matriz[$i][$j] = rand(1, 100);
            }
        }
        return $matriz;
    }

    if(isset($_POST['generar'])){
        $filas = $_POST['filas'];
        $columnas = $_POST['columnas'];

        $matriz = generarMatriz($filas, $columnas);

        echo "<table border='1'>";
        foreach ($matriz as $fila) {
            echo "<tr>";
            foreach ($fila as $valor) {
                echo "<td>$valor</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }
    ?>
</body>
</html>
