<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Iniciar Sesión</title>
</head>
<body>
    <h1>Iniciar Sesión</h1>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $usuario = $_POST['usuario'];
        $contrasena = $_POST['contrasena'];

        $archivo = fopen('usuarios.txt', 'r');
        $acceso = false;

        if ($archivo) {
            while (($linea = fgets($archivo)) !== false) {
                list($nombre, $pass) = explode('|', trim($linea));
                if ($nombre === $usuario && $pass === $contrasena) {
                    $acceso = true;
                    session_start();
                    $_SESSION['usuario'] = $usuario;
                    header('Location: bienvenida.php');
                    exit();
                }
            }
            fclose($archivo);
        }

        if (!$acceso) {
            echo 'Error de acceso. Usuario o contraseña incorrectos.';
        }
    }
    ?>

    <form action="" method="post">
        <label for="usuario">Usuario:</label>
        <input type="text" id="usuario" name="usuario" required><br><br>
        <label for="contrasena">Contraseña:</label>
        <input type="password" id="contrasena" name="contrasena" required><br><br>
        <button type="submit">Iniciar Sesión</button>
    </form>
</body>
</html>
