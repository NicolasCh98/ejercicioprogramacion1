<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Ejercicio 19</title>
</head>

<body>
    <?php
// Abrir el archivo de entrada
$entrada = fopen('notas.txt', 'r');

if (!$entrada) {
    die('No se pudo abrir el archivo de entrada.');
}

// Abrir el archivo de salida
$salida = fopen('notasFinales.txt', 'w');

if (!$salida) {
    fclose($entrada);
    die('No se pudo abrir el archivo de salida.');
}

// Procesa el archivo de entrada y carga en el archivo de salida
while (($linea = fgets($entrada)) !== false) {
    $datos = explode(' ', $linea);
    $matricula = $datos[0];
    $notas = array_slice($datos, 3); // Tomar las notas (las tres últimas partes)
    $sumaNotas = array_sum($notas); // Calcular la sumatoria de las notas
    
    fwrite($salida, "$matricula $sumaNotas\n");
}

fclose($entrada);
fclose($salida);

echo 'Proceso completado. Se ha creado el archivo con las notas finales.';
?>
</body>

</html>