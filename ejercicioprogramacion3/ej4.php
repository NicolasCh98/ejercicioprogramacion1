<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Secuencia de Números</title>
</head>
<body>
    <h1>Secuencia de Números Ordenados</h1>

    <?php
    // Generar valores aleatorios entre 50 y 900 usando mt_srand y mt_rand
    mt_srand(); // Semilla aleatoria
    $numero1 = mt_rand(50, 900);
    $numero2 = mt_rand(50, 900);
    $numero3 = mt_rand(50, 900);

    // Ordenar los números de mayor a menor
    $numeros = [$numero1, $numero2, $numero3];
    rsort($numeros);

    // Imprimir los números en pantalla con colores
    echo "<p>Secuencia de números ordenados: ";
    echo "<span style='color:green;'>{$numeros[0]}</span> ";
    echo "{$numeros[1]} ";
    echo "<span style='color:red;'>{$numeros[2]}</span></p>";
    ?>
</body>
</html>
