<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Ejercicio 17</title>
</head>

<body>
    <?php

$arbol = array(
    'R' => array(
        'S' => array(
            'U' => array('L', 'Q', 'W'),
            'V'
        ),
        'T' => array(
            'G' => array('C', 'K'),
            'F'
        ),
        'D' => array(
            'H' => array('A', 'X'),
            'J' => array('Z', 'I')
        )
    )
);

function imprimirArbol($arbol, $nivel = 0) {
    foreach ($arbol as $nodo => $hijos) {
        echo str_repeat("&nbsp;&nbsp;&nbsp;", $nivel * 2) . $nodo . "<br>";
        if (is_array($hijos)) {
            imprimirArbol($hijos, $nivel + 1);
        }
    }
}
// Visualizar el árbol
imprimirArbol($arbol);

?>
</body>

</html>