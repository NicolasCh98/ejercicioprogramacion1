<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Bienvenido</title>
</head>
<body>
    <?php
    session_start();
    if (isset($_SESSION['usuario'])) {
        echo '<h1>Bienvenido, ' . htmlspecialchars($_SESSION['usuario']) . '!</h1>';
        echo '<p>Has iniciado sesión correctamente.</p>';
    } else {
        echo '<h1>Error de inicio de sesión</h1>';
        echo '<p>Por favor, inicia sesión desde el <a href="index.php">formulario de inicio de sesión</a>.</p>';
    }
    ?>
</body>
</html>
