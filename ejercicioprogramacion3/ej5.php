<?php
// Inicializar variables
$nombre = "";
$apellido = "";
$edad = "";

// Procesar el formulario si se ha enviado
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = htmlspecialchars($_POST["nombre"]);
    $apellido = htmlspecialchars($_POST["apellido"]);
    $edad = htmlspecialchars($_POST["edad"]);
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>Formulario</title>
</head>
<body>
    <h1>Formulario de Usuario</h1>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
        <label for="nombre">Nombre:</label>
        <input type="text" id="nombre" name="nombre" required><br>
        
        <label for="apellido">Apellido:</label>
        <input type="text" id="apellido" name="apellido" required><br>
        
        <label for="edad">Edad:</label>
        <input type="number" id="edad" name="edad" required><br>
        
        <input type="submit" value="Enviar">
    </form>

    <?php
    // Mostrar datos del formulario procesados
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        echo <<<HTML
        <h2>Datos del formulario enviado:</h2>
        <p>Nombre: $nombre</p>
        <p>Apellido: $apellido</p>
        <p>Edad: $edad</p>
HTML;
    }
    ?>
</body>
</html>
