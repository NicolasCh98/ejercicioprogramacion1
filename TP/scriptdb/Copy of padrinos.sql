CREATE TABLE "Usuario" (
  "id" integer PRIMARY KEY,
  "nombre_usuario" varchar,
  "contrasena" varchar,
  "correo_electronico" varchar,
  "rol" varchar
);

CREATE TABLE "AnimalRescatado" (
  "id" integer PRIMARY KEY,
  "nombre" varchar,
  "especie" varchar,
  "raza" varchar,
  "edad" integer,
  "estado" varchar,
  "fecha_rescate" timestamp,
  "informacion_adicional" text
);

CREATE TABLE "Adoptante" (
  "id" integer PRIMARY KEY,
  "nombre" varchar,
  "apellido" varchar,
  "correo_electronico" varchar,
  "telefono" varchar,
  "direccion" varchar,
  "ocupacion" varchar,
  "informacion_de_adopcion" text
);

CREATE TABLE "Voluntario" (
  "id" integer PRIMARY KEY,
  "nombre" varchar,
  "apellido" varchar,
  "correo_electronico" varchar,
  "telefono" varchar,
  "direccion" varchar,
  "disponibilidad" varchar,
  "areas_de_interes" text
);

CREATE TABLE "Egresos" (
  "id_egresos" integer PRIMARY KEY,
  "descripcion" text,
  "monto" decimal,
  "fecha_de_gasto" timestamp,
  "categoria" varchar,
  "id_del_usuario" integer,
  "id_tipo_egreso" integer,
  "id_fuente" integer
);

CREATE TABLE "TipoEgresos" (
  "id_tipo_egreso" integer PRIMARY KEY,
  "descripcion" varchar
);

CREATE TABLE "Donacion" (
  "id" integer PRIMARY KEY,
  "donante" integer,
  "cantidad" decimal,
  "fecha_donacion" timestamp,
  "monto" decimal,
  "metodo_de_donacion" varchar,
  "comentarios" text,
  "id_del_usuario" integer,
  "id_tipo_donacion" integer
);

CREATE TABLE "TipoDonacion" (
  "id_tipo_donacion" integer PRIMARY KEY,
  "descripcion" text
);

CREATE TABLE "EventoDeRecaudacionDeFondos" (
  "id" integer PRIMARY KEY,
  "nombre_del_evento" varchar,
  "fecha" timestamp,
  "ubicacion" varchar,
  "descripcion" text,
  "estado" varchar,
  "id_del_usuario" integer
);

CREATE TABLE "SolicitudDeAdopcion" (
  "id" integer PRIMARY KEY,
  "id_del_usuario" integer,
  "id_del_adoptante" integer,
  "id_del_animal" integer
);

CREATE TABLE "Ingresos" (
  "id" integer PRIMARY KEY,
  "descripcion" text,
  "monto" decimal,
  "fecha" timestamp,
  "id_fuente" integer,
  "tipo" varchar,
  "id_del_usuario" integer,
  "id_tipo_ingreso" integer
);

CREATE TABLE "TipoIngresos" (
  "id_tipo_ingreso" integer PRIMARY KEY,
  "descripcion" varchar
);

CREATE TABLE "Fuente" (
  "id_fuente" integer PRIMARY KEY,
  "id_del_evento_de_recaudacion" integer,
  "id_de_donacion" integer
);

COMMENT ON COLUMN "Ingresos"."tipo" IS 'Donación, Evento de Recaudación, u otro tipo de ingreso';

ALTER TABLE "Egresos" ADD FOREIGN KEY ("id_del_usuario") REFERENCES "Usuario" ("id");

ALTER TABLE "Egresos" ADD FOREIGN KEY ("id_tipo_egreso") REFERENCES "TipoEgresos" ("id_tipo_egreso");

ALTER TABLE "Egresos" ADD FOREIGN KEY ("id_fuente") REFERENCES "Fuente" ("id_fuente");

ALTER TABLE "Donacion" ADD FOREIGN KEY ("donante") REFERENCES "Voluntario" ("id");

ALTER TABLE "Donacion" ADD FOREIGN KEY ("id_del_usuario") REFERENCES "Usuario" ("id");

ALTER TABLE "Donacion" ADD FOREIGN KEY ("id_tipo_donacion") REFERENCES "TipoDonacion" ("id_tipo_donacion");

ALTER TABLE "EventoDeRecaudacionDeFondos" ADD FOREIGN KEY ("id_del_usuario") REFERENCES "Usuario" ("id");

ALTER TABLE "SolicitudDeAdopcion" ADD FOREIGN KEY ("id_del_usuario") REFERENCES "Usuario" ("id");

ALTER TABLE "SolicitudDeAdopcion" ADD FOREIGN KEY ("id_del_adoptante") REFERENCES "Adoptante" ("id");

ALTER TABLE "SolicitudDeAdopcion" ADD FOREIGN KEY ("id_del_animal") REFERENCES "AnimalRescatado" ("id");

ALTER TABLE "Ingresos" ADD FOREIGN KEY ("id_fuente") REFERENCES "Fuente" ("id_fuente");

ALTER TABLE "Ingresos" ADD FOREIGN KEY ("id_del_usuario") REFERENCES "Usuario" ("id");

ALTER TABLE "Ingresos" ADD FOREIGN KEY ("id_tipo_ingreso") REFERENCES "TipoIngresos" ("id_tipo_ingreso");

ALTER TABLE "Fuente" ADD FOREIGN KEY ("id_del_evento_de_recaudacion") REFERENCES "EventoDeRecaudacionDeFondos" ("id");

ALTER TABLE "Fuente" ADD FOREIGN KEY ("id_de_donacion") REFERENCES "Donacion" ("id");
