import React from "react";
import whatsappLogo from "../../assets/whatsapp-icon.png";

export default function Ayudanos() {
  return (
    <div className="bg-[#143f6e] h-screen flex justify-center py-[30px]">
      <div className="container text-white mx-[100px]">
        <h1 className="text-7xl font-bold text-center pb-[30px]">Ayudanos</h1>
        <div className="grid grid-cols-2 auto-rows-auto gap-5 text-center">
          <div className="content-center pt-8">
            <h2 className="text-5xl">Transferencias</h2>
            <div className="text-2xl text-left pt-8">
              <p>Banco: <b>Familiar</b></p>
              <p>Titular: <b>Daniel Armando Reyes</b></p>
              <p>CI: <b>5420392</b></p>
              <p>Nro. de Cuenta desde Familiar: <b>81-1001647</b></p>
              <p>Nro. de cuenta desde otros bancos: <b>811001647</b></p>
            </div>
          </div>
          <div className="pt-8">
            <h2 className="text-5xl">Donaciones</h2>
            <div className="text-2xl text-left pt-8">
              <p>Aceptamos cualquier medicamento, balanceado, ropa, etc.</p>
              <p>Contacto: <a target="_blank" href="https://wa.me/+595981755022" className="hover:text-black"><b>Tamara Vallejo</b></a></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
