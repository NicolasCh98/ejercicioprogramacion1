import React, { useEffect, useState } from "react";
import EventCard from "../../components/eventCard";
import eventosServices from "../../services/eventosServices";
import { useAuth } from "../../context/authProvider";

export default function Eventos() {
  // const eventos = [
  //   {
  //     imagen: "/src/assets/evento-hamburgueseada.jpeg",
  //     titulo: "Hamburgueseada",
  //     lugar: "Rio Parana 360 entre felicidad y cacique sepe",
  //     descripcion: "",
  //   },
  //   {
  //     imagen: "/src/assets/evento-postres.jpeg",
  //     titulo: "Postres",
  //     lugar: "Plaza Techada",
  //     descripcion: "",
  //   },
  //   {
  //     imagen: "/src/assets/evento-colecta.jpeg",
  //     titulo: "Colecta",
  //     lugar: "Puntos de Colecta",
  //     descripcion: "",
  //   },
  // ];

  const { eventosCount } = useAuth();
  const [ eventoData, setEventoData ] = useState([]);
  const [ eventos, setEventos ] = useState([]);
  const [ cantRegistros, setCantRegistros ] = useState(eventosCount);
  const [ isLoading, setIsLoading ] = useState(true);

  useEffect(() => {
    const fetchEventos = async () => {
      try {
        const eventosData = await eventosServices.GetAllEventos();
        setEventoData(eventosData);
      } catch (error) {
        console.log("Error al obtener eventos:", error);
      }
    };
    fetchEventos();
  }, []);

  useEffect(() => {
    setEventos(mappedData(eventoData));
    setTimeout(() => {
      setIsLoading(false)
    }, 2000)
  }, [eventoData]);

  const mappedData = (data) => {
    const datMap = data.map(evento => ({
      id: evento.id,
      imagen: evento.imagen_path,
      titulo: evento.nombre_del_evento,
      lugar: evento.ubicacion,
      descripcion: evento.descripcion
    }))
    return datMap
  }

  return (
    <div className="bg-[#143f6e] h-screen flex justify-center py-[30px]">
      <div className="container text-white mx-[100px]">
        <h1 className="text-7xl font-bold text-center pb-[30px]">Eventos</h1>
        <div className="grid grid-cols-3 auto-rows-auto gap-5">
          {eventos.map((evento: any, index: number) => {
            return (
              <EventCard
                key={index}
                imagen={evento.imagen}
                titulo={evento.titulo}
                lugar={evento.lugar}
                descripcion={evento.descripcion}
              />
            );
          })}
        </div>
      </div>
    </div>
  );
}
