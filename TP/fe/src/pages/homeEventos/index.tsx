
import Dform from "../../components/dform/dForm";

import React, { useEffect, useState } from "react";
import UTable from "../../components/uTable/utable";
import SimpleDialog from "../../components/simpleDialog/simpleDialog";
import eventosServices from "../../services/eventosServices";
import loginService from "../../services/loginService";
import { useAuth } from "../../context/authProvider";



export default function HomeEventos() {
    const [eventosData, setEventosData] = useState([]);
    const [error, setError] = useState(null);

    const fetchData = async () => {
        try {
            const data = await eventosServices.GetAllEventos();
            setEventosData(data.map(item => ({
                id: item.id,
                nombre_del_evento: item.nombre_del_evento,
                fecha: item.fecha,
                image: item.imagen_path,
                ubicacion: item.ubicacion,
                descripcion: item.descripcion,
            })));
            console.log("prueba", data);
        } catch (error) {
            setError(error.message);
        }
    };

    const columns = [
        { header: "Id", accessorKey: "id" },
        { header: "Nombre del Evento", accessorKey: "nombre_del_evento" },
        { header: "Fecha del Evento", accessorKey: "fecha" },
        { header: "Ubicacion", accessorKey: "ubicacion" },
        { header: "Descripcion", accessorKey: "descripcion" },
        { header: "Imagen", accessorKey: "image", render: (row) => <img src={row.image} alt="Animal" style={{ width: "50px" }} /> },
        // Puedes agregar más columnas según sea necesario
    ];

    const inputConfig = [
        {
            name: "nombre_del_evento",
            type: "text",
            placeholder: "Nombre del Evento",
            validations: { required: "El nombre del evento es requerido" },
        },
        {
            name: "fecha",
            type: "date",
            placeholder: "Fecha",
            validations: { required: "La fecha del evento es requerida" },
        },
        {
            name: "ubicacion",
            type: "text",
            placeholder: "Ubicacion",
            validations: { required: "La ubicacion del evento es requerida" },
        },
        {
            name: "descripcion",
            type: "text",
            placeholder: "Descripcion",
            validations: { required: "La descripcion del evento es requerida" },
        },
        {
            name: "image",
            type: "file",
            placeholder: "Imagen",
            validations: {},
        }
        // Puedes agregar más campos según sea necesario
    ];


    const initialValues = {
        id_del_usuario: "",
        nombre_del_evento: "",
        fecha: "",
        ubicacion: "",
        descripcion: "",
        image: "",
        // Puedes agregar más campos según sea necesario
    };

    const context = useAuth();

    const onSubmit = async (data) => {
        // console.log("PRUEBA", data)
        const file = data.image[0];
        const user = await loginService.GetUser(context.token);

        if (file) {
            const imageData = await readImageFile(file);
            data.image = imageData;
        }

        console.log(data);
        const mappedData = {
            nombre_del_evento: data.nombre_del_evento,
            fecha: data.fecha,
            ubicacion: data.ubicacion,
            descripcion: data.descripcion,
            estado: "Registrado",
            id_usuario: user.user.id,
            id_red: 1,
            dias: "1",
            image: data.image === "" ? "NULL": data.image, // Aún no tenemos la imagen, la llenaremos más adelante
        };
        console.log(mappedData);

        try {
            const res = await eventosServices.CreateEvento(mappedData);
            console.log(res);
            await fetchData();
        } catch (error) {
            console.error("Error al crear el evento:", error);
        }
    };

    const onEdit = async (data) => {
        console.log("PRUEBA", data)
        const file = data.image_path[0];

        if (file) {
            const imageData = await readImageFile(file);
            data.image = imageData;
        }

        console.log(data);

        // try {
        //     const res = await eventosServices.EditEvento(data.id, data);
        //     console.log(res);
        //     await fetchData();
        // } catch (error) {
        //     console.error("Error al crear el evento:", error);
        // }
    };

    const readImageFile = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = (e) => {
                resolve(e.target.result);
            };

            reader.onerror = (error) => {
                reject(error);
            };

            reader.readAsDataURL(file);
        });
    };

    const onDelete = async (id) => {
        try {
            const data = await eventosServices.deleteEvento(id);
            console.log(id)
            fetchData()
        } catch (error) {
            setError(error.message)
        }
    }


    useEffect(() => {


        fetchData();
    }, []);


    return (
        <div className="bg-all py-20 text-[#0e3d69] h-screen">
            <h1 className="text-center text-6xl font-bold pb-10">Eventos</h1>
            <div className="grid gap-y-4  mx-[200px]">

            </div>
            <div className="grid pt-10">
                <div className="flex flex-col items-center justify-center text-center">
                    <div className="flex items-start w-9/12 pb-2">
                        <SimpleDialog buttonText="Crear">
                            <Dform initialValues={initialValues} inputs={inputConfig} onSub={onSubmit} />

                        </SimpleDialog>
                    </div>
                </div>
                <div className="flex flex-col items-center justify-center text-center">

                    <div className="w-9/12">
                        <UTable data={eventosData} columns={columns} initialValues={initialValues} inputs={inputConfig} onDel={onDelete} onEdit={onEdit}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

