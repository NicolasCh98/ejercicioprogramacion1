import React from "react";
import { useLocation } from "react-router-dom";

export default function AnimalDetalle() {
    const location = useLocation();
    const { imagen, nombre, descripcion, slug, texto } = location.state.animal;
    return (
        <div className="h-screen bg-[#143f6e] flex  justify-center py-[30px]">
            <div className="container text-white">
                <h1 className="text-7xl font-bold text-center pb-[30px]">{nombre}</h1>
                <div className="grid grid-cols-2  gap-5 content-center">
                    <img className="" src={imagen} />
                    <p className="text-xl">{texto}</p>
                </div>
            </div>
        </div>
    )
}