import { Alert, Card, CardBody, CardFooter, CardHeader } from "@material-tailwind/react";
import React, { useEffect, useRef, useState } from "react";
import Dform from "../../components/dform/dForm";
import animalService from "../../services/animalService";
import FormSinSubmit from "../../components/formSinSubmit/formSinSubmit";
import FormSinSubmit2 from "../../components/formSinSubmit/formSinSubmit2";
import { useNavigate, useParams } from "react-router-dom";
import adoptanteService from "../../services/adoptanteService";
import solicitudService from "../../services/solicitudService";
// Asegúrate de importar tu componente Card

function Icon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="0 0 24 24"
            fill="currentColor"
            className="h-6 w-6"
        >
            <path
                fillRule="evenodd"
                d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z"
                clipRule="evenodd"
            />
        </svg>
    );
}

function IconFail() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            strokeWidth={2}
            stroke="currentColor"
            className="h-6 w-6"
        >
            <path
                strokeLinecap="round"
                strokeLinejoin="round"
                d="M11.25 11.25l.041-.02a.75.75 0 011.063.852l-.708 2.836a.75.75 0 001.063.853l.041-.021M21 12a9 9 0 11-18 0 9 9 0 0118 0zm-9-3.75h.008v.008H12V8.25z"
            />
        </svg>
    );
}

const SolicitudPublica = () => {
    const params = useParams();
    const [currentStep, setCurrentStep] = useState(1);
    const [animalData, setAnimalData] = useState([]);
    const [firstStepData, setFirstStepData] = useState({
        ci: "",
        nombre: "",
        apellido: "",
        correo_electronico: "",
        telefono: "",
        direccion: "",
        ocupacion: "",
    });
    const [secondStepData, setSecondStepData] = useState({
        Animal: params.id,
        datos_adicionales: "",
    });
    const [mergedData, setMergedData] = useState([]);
    const [initialValues1, setInitialValues1] = useState({
        ci: "",
        nombre: "",
        apellido: "",
        correo_electronico: "",
        telefono: "",
        direccion: "",
        ocupacion: "",
    })
    const [initialValues2, setInitialValues2] = useState({
        Animal: params.id,
        datos_adicionales: "",
    })
    const dformRef1 = useRef();
    const dformRef2 = useRef();
    const navigate = useNavigate();

    const [mostrarAlerta, setMostrarAlerta] = useState(false);

    const [claseAnimacion, setClaseAnimacion] = useState('animate__fadeInUp');

    const [mostrarAlertaExito, setMostrarAlertaExito] = useState(false);
    const [mostrarAlertaError, setMostrarAlertaError] = useState(false);
    const [errorMsg, setErrorMsg] = useState("Ha ocurrido un error al enviar la solicitud.");

    useEffect(() => {
        let timeoutMostrar;
        let timeoutOcultar;

        if (mostrarAlertaExito) {
            // Mostrar alerta con clase animate__fadeInUp
            timeoutMostrar = setTimeout(() => {
                // Cambiar la clase después de 3 segundos
                setClaseAnimacion('animate__fadeOutDown');

                // Ocultar la alerta después de 2 segundos
                timeoutOcultar = setTimeout(() => {
                    setMostrarAlertaExito(false);
                    setClaseAnimacion('animate__fadeInUp');
                }, 1000);
            }, 3000);
        }

        // Limpieza de timeouts
        return () => {
            clearTimeout(timeoutMostrar);
            clearTimeout(timeoutOcultar);
        };
    }, [mostrarAlertaExito]);


    useEffect(() => {
        let timeoutMostrar;
        let timeoutOcultar;

        if (mostrarAlertaError) {
            // Mostrar alerta con clase animate__fadeInUp
            timeoutMostrar = setTimeout(() => {
                // Cambiar la clase después de 3 segundos

                setClaseAnimacion('animate__fadeOutDown');

                // Ocultar la alerta después de 2 segundos
                timeoutOcultar = setTimeout(() => {
                    setMostrarAlertaError(false);
                    setClaseAnimacion('animate__fadeInUp');
                }, 1000);
            }, 3000);
        }

        // Limpieza de timeouts
        return () => {
            clearTimeout(timeoutMostrar);
            clearTimeout(timeoutOcultar);
        };
    }, [mostrarAlertaError]);


    const handleNextStep = async () => {
        // Llama a onSubmitPrev y espera a que se complete
        console.log("CURRENT STEP", currentStep)
        console.log("PSDKJFLKJSDFLKSDJFLSFJS", params)
        try {
            if (currentStep == 1) {
                // await dformRef1.current.onSubmitPrev();

                // Ahora isValid contiene el resultado
                const isValid = dformRef1.current.isValid;
                const formDataFromDform = await dformRef1.current.onSubmit();
                if (isValid) {
                    console.log(currentStep)
                    setCurrentStep((prevStep) => prevStep + 1);
                    console.log(currentStep)
                } else {

                }
                console.log("Datos del formulario de Dform:", isValid);

            }
            if (currentStep == 2) {
                // await dformRef2.current.onSubmitPrev();

                // Ahora isValid contiene el resultado
                const isValid = dformRef2.current.isValid;

                if (isValid) {

                    const formDataFromDform = await dformRef2.current.onSubmit();
                    setCurrentStep((prevStep) => prevStep + 1);


                } else {

                }

                console.log("Datos del formulario de Dform:", isValid);
            }

        } catch (error) {
            console.error("Error al obtener datos del formulario:", error);
        }
        // Continúa con el siguiente paso si es necesario
        // setCurrentStep((prevStep) => prevStep + 1);
    };

    const formatSolicitud = (id) => {
        const fechaActual = new Date();

        // Obtener los componentes de la fecha
        const año = fechaActual.getFullYear();
        const mes = String(fechaActual.getMonth() + 1).padStart(2, '0'); // Sumar 1 porque los meses van de 0 a 11
        const dia = String(fechaActual.getDate()).padStart(2, '0');

        // Formatear la fecha como "yyyy-mm-dd"
        const fechaFormateada = `${año}-${mes}-${dia}`;

        const solicitud = {
            "id_del_adoptante": id,
            "id_del_animal": parseInt(secondStepData.Animal ?? '0', 10),
            "fecha_solicitud": fechaFormateada,
            "datos_adicionales": secondStepData.datos_adicionales,
            "estado": "Registrado"
        };

        return solicitud;
    }



    const handleSave = () => {
        let adoptanteId;

        // Crear el adoptante
        adoptanteService.CreateAdoptante(firstStepData)
            .then((adoptanteResponse) => {
                console.log("ADOPTANTE", adoptanteResponse)
                // Obtener el ID del adoptante creado
                adoptanteId = adoptanteResponse.id; // Ajusta según la respuesta real del servidor

                // Formatear la solicitud con el ID del adoptante
                const solicitudData = formatSolicitud(adoptanteId);
                console.log(solicitudData)

                // Crear la solicitud usando el servicio de solicitudService
                return solicitudService.CreateSolicitud(solicitudData);
            })
            .then((solicitudResponse) => {
                // Manejar la respuesta de la creación de la solicitud, si es necesario
                console.log('Respuesta de creación de solicitud:', solicitudResponse);
                setMostrarAlertaExito(true)
                setTimeout(() => {
                    navigate('/animales');
                  }, 2000);
            })
            .catch((error) => {
                // Manejar cualquier error que pueda ocurrir
                setMostrarAlertaError(true)
                console.error('Error al enviar los datos:', error);
                setErrorMsg(error.toString())
                console.log(error.toString())
                console.log(mostrarAlertaError)
                console.log(mostrarAlertaExito)

                // Si ocurrió un error, revertir la creación del adoptante
                if (adoptanteId) {
                    revertirCreacionAdoptante(adoptanteId);
                }
            });
    };

    const revertirCreacionAdoptante = (adoptanteId) => {
        // Llama al servicio para eliminar el adoptante
        adoptanteService.DeleteAdoptante(adoptanteId)
            .then((response) => {
                console.log(response);
                // Manejar la respuesta del servicio DeleteAdoptante, si es necesario
            })
            .catch((error) => {
                // Manejar cualquier error que pueda ocurrir al eliminar el adoptante
                console.error('Error al revertir la creación del adoptante:', error);
            });
    };

    const handlePrevStep = () => {

        setCurrentStep((prevStep) => Math.max(1, prevStep - 1));
    };

    const inputConfig1 = [
        {
            name: "ci",
            type: "text",
            placeholder: "Cédula de Identidad",
            validations: {
                required: "La cédula de identidad es requerida",
                pattern: {
                    value: /^[0-9]+$/,
                    message: "La cédula de identidad debe contener solo números",
                },
            },
        },
        {
            name: "nombre",
            type: "text",
            placeholder: "Nombre",
            validations: {
                required: "El nombre es requerido",
                maxLength: {
                    value: 80,
                    message: "El nombre no puede exceder los 80 caracteres",
                },
            },
        },
        {
            name: "apellido",
            type: "text",
            placeholder: "Apellido",
            validations: {
                required: "El apellido es requerido",
                maxLength: {
                    value: 80,
                    message: "El apellido no puede exceder los 80 caracteres",
                },
            },
        },
        {
            name: "correo_electronico",
            type: "email",
            placeholder: "Correo Electrónico",
            validations: {
                required: "El correo electrónico es requerido",
                pattern: {
                    value: /^\S+@\S+$/i,
                    message: "Ingresa un correo electrónico válido",
                },
            },
        },
        {
            name: "telefono",
            type: "number",
            placeholder: "Teléfono",
            validations: {
                required: "El teléfono es requerido",
                pattern: {
                    value: /^[0-9]+$/,
                    message: "Ingresa un número de teléfono válido",
                },
            },
        },
        {
            name: "direccion",
            type: "text",
            placeholder: "Dirección",
            validations: { required: "La dirección es requerida" },
        },
        {
            name: "ocupacion",
            type: "text",
            placeholder: "Ocupación (opcional)",
        },
    ];

    // const initialValues1 = {
    //     ci: "",
    //     nombre: "",
    //     apellido: "",
    //     correo_electronico: "",
    //     telefono: "",
    //     direccion: "",
    //     ocupacion: "",
    // };


    const fetchData = async () => {

        try {
            const data = await animalService.GetAllAnimals();
            setAnimalData(data.map(item => ({
                value: item.id,
                label: item.nombre,
            })));
            console.log("prueba", data)
        } catch (error) {
            // setError(error.message);
            console.log(error)
        }
    };

    useEffect(() => {


        fetchData();
    }, []);

    // const initialValues2 = {
    //     Animal: "",
    //     datos_adicionales: "",
    // };

    // inputConfig para el formulario de solicitud de adopción
    const inputConfig2 = [
        {
            name: "Animal",
            type: "select",
            placeholder: "Animal",
            validations: { required: "El Animal es requerido" },
            options: animalData,
        },
        {
            name: "datos_adicionales",
            type: "text",
            placeholder: "Datos Adicionales",
            validations: { required: "Los Datos Adicionales son requeridos" },
        },

    ];
    const onSubmit = (data) => {
        // if (currentStep == 1) {
        setFirstStepData(data)
        setInitialValues1(data)
        console.log("Step 1", firstStepData)
        // }
        // if (currentStep == 2) {
        // console.log("Step 2", data)
        // }
    }

    const onSubmit2 = (data) => {
        // if (currentStep == 1) {
        //     console.log("Step 1", data)
        // }
        // if (currentStep == 2) {
        setSecondStepData(data)
        setInitialValues2(data)
        console.log("Step 1", firstStepData)
        console.log("Step 2", data)
        setMergedData({
            ...firstStepData,
            ...data
        })
        // }
    }


    const renderForm = () => {
        switch (currentStep) {
            case 1:
                return <FormSinSubmit ref={dformRef1} title={"Datos del adoptante"} initialValues={initialValues1} inputs={inputConfig1} onSub={onSubmit} />;
            case 2:
                return <FormSinSubmit2 ref={dformRef2} title={"Solicitud de adopcion"} initialValues={initialValues2} inputs={inputConfig2} onSub={onSubmit2} />;
            case 3:
                return <FormStep3 data={mergedData} animalData={animalData} />;
            default:
                return null;
        }
    };

    return (
        <div className="bg-all py-20 text-[#0e3d69] min-w-max">
            <h1 className="text-center text-6xl font-bold pb-10">Adopta</h1>

            <div className="grid mx-[200px]"></div>
            <div className="grid pt-10">
                <div className="flex flex-col items-center justify-center text-center ">


                    <Card color="transparent" shadow={false} className="w-4/12">
                        <CardHeader color="transparent" shadow={false}>
                            <h2 className="text-3xl font-bold mb-6">Formulario de Solicitud de Adopción</h2>
                            <div className="relative flex items-center justify-between w-full">
                                {[1, 2, 3].map((step) => (
                                    <div
                                        key={step}
                                        className={`relative z-10 grid w-10 h-10 font-bold text-white transition-all duration-300 ${step === currentStep ? "bg-gray-900" : "bg-gray-400"
                                            } rounded-full place-items-center`}
                                    >
                                        {step}
                                    </div>
                                ))}
                            </div>
                        </CardHeader>

                        <div className="w-full px-8 py-4 max-w-full">
                            {/* Indicador de paso */}

                            {/* Formulario actual */}
                            <CardBody className="w-full mx-auto max-w-full">
                                {currentStep === 3 && (
                                    <div style={{ height: mostrarAlertaExito || mostrarAlertaError ? 'auto' : '56px'}} className="mb-2">
                                        {mostrarAlertaExito && (
                                            <Alert
                                                icon={<Icon />}
                                                className={`animate__animated ${claseAnimacion} max-w-full rounded-none rounded-t border-l-4 border-[#2ec946] bg-[#2ec946]/10 font-medium text-[#2ec946]`}
                                            >
                                                Solicitud enviada satisfactoriamente
                                            </Alert>
                                        )}

                                        {/* Alerta de error */}
                                        {mostrarAlertaError && (
                                            <Alert
                                                icon={<IconFail />}
                                                className={`animate__animated ${claseAnimacion} rounded-none rounded-t border-l-4 border-[#c92e2e] bg-[#c92e2e]/10 font-medium text-[#c92e2e] overflow-hidden`}
                                            >
                                                <p className="text-clip overflow-hidden ...">{errorMsg}</p>
                                            </Alert>
                                        )}
                                    </div>
                                )}

                                {renderForm()}
                            </CardBody>
                            {/* Botones de navegación */}
                            <CardFooter>
                                <div className="flex justify-between mt-16">
                                    <button
                                        className="select-none rounded-lg bg-gray-900 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-gray-900/10 transition-all hover:shadow-lg hover:shadow-gray-900/20 focus:opacity-[0.85] focus:shadow-none active:opacity-[0.85] active:shadow-none disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                                        type="button"
                                        onClick={handlePrevStep}
                                        disabled={currentStep === 1}
                                    >
                                        Prev
                                    </button>
                                    {currentStep !== 3 ? (
                                        <button
                                            className="select-none rounded-lg bg-gray-900 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-gray-900/10 transition-all hover:shadow-lg hover:shadow-gray-900/20 focus:opacity-[0.85] focus:shadow-none active:opacity-[0.85] active:shadow-none disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                                            type="button"
                                            onClick={handleNextStep}
                                            disabled={currentStep === 3}
                                        >
                                            Next
                                        </button>
                                    ) : (
                                        <button
                                            className="select-none rounded-lg bg-blue-400 py-3 px-6 text-center align-middle font-sans text-xs font-bold uppercase text-white shadow-md shadow-gray-900/10 transition-all hover:shadow-lg hover:shadow-gray-900/20 focus:opacity-[0.85] focus:shadow-none active:opacity-[0.85] active:shadow-none disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                                            type="button"
                                            onClick={handleSave}
                                        // Puedes ajustar el evento y estilos según tu necesidad para el caso de Step 3
                                        >
                                            Finalizar
                                        </button>
                                    )}
                                </div>
                            </CardFooter>
                        </div>
                    </Card>
                </div>
            </div>
        </div>
    );
};


const FormStep2 = () => {



    return (

        <div>
            {/* Contenido del formulario para el paso 2 */}
            {/* <Dform title={"Solicitud de adopcion"} initialValues={initialValues} inputs={inputConfig} onSub={onSubmit} mostrarSubmit={false} /> */}
        </div>
    )
};

const FormStep3 = ({ data, animalData }) => {
    // const data = {
    //     ci: '2323',
    //     nombre: 'Marcelo',
    //     apellido: 'Ramirez',
    //     correo_electronico: 'marcee98@gmail.com',
    //     telefono: '0983161511',
    //     datos_adicionales: '3232',
    //     direccion: 'Lambare',
    //     ocupacion: '',
    //     Animal: '20',
    //     // ... otras propiedades
    // };

    const getAnimalName = (animalId) => {
        const numericAnimalId = parseInt(animalId, 10);
        const animal = animalData.find((a) => a.value === numericAnimalId);
        return animal ? animal.label : 'Desconocido';
    };

    function formateaNombre(texto) {
        // Divide el texto en palabras utilizando "_"
        const palabras = texto.split('_');

        // Capitaliza la primera letra de cada palabra
        const palabrasCapitalizadas = palabras.map(palabra => {
            return palabra.charAt(0).toUpperCase() + palabra.slice(1);
        });

        // Une las palabras con espacios
        const resultado = palabrasCapitalizadas.join(' ');

        return resultado + ' :';
    }

    const renderTableRows = () => {
        return (
            <tbody>
                {Object.entries(data).map(([title, value]) => (
                    <tr key={title}>
                        <td className="p-4 border-b border-blue-gray-50"><p className="font-semibold">
                            {formateaNombre(title)}
                        </p></td>
                        <td className="p-4 border-b border-blue-gray-50"><p className="block font-sans text-sm text-gray-600 antialiased font-light leading-relaxed text-inherit">{title === 'Animal' ? getAnimalName(value) : value}</p></td>
                    </tr>
                ))}
            </tbody>
        );
    };

    return (
        <div
            className="relative flex flex-col w-full h-full text-gray-700 bg-white shadow-md rounded-b-xl bg-clip-border">
            <table className="w-full text-left table-auto min-w-max">
                <caption className=" bg-gray-100 caption pt-5 pb-5 font-sans text-md antialiased font-semibold leading-relaxed tracking-normal text-inherit">
                    Solicitud de adopcion
                </caption>
                <thead>
                    <tr>
                        <th className="p-4 border-b border-r border-blue-gray-400 bg-transparent font-sans text-md antialiased font-semibold leading-relaxed text-inherit">#</th>
                        <th className="p-4 border-b border-l border-blue-gray-400 bg-transparent font-sans text-md antialiased font-semibold leading-relaxed text-inherit ">Valor</th>
                    </tr>
                </thead>
                {renderTableRows()}
            </table>
        </div>
    );
}


export default SolicitudPublica;
