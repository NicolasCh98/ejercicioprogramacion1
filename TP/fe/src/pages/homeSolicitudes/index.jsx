
import Dform from "../../components/dform/dForm";

import React, { useEffect, useState } from "react";
import UTable from "../../components/uTable/utable";
import SimpleDialog from "../../components/simpleDialog/simpleDialog";
import solicitudService from "../../services/solicitudService";



export default function HomeSolicitudes() {
    const [solicitudesData, setSolicitudesData] = useState([]);
    const [error, setError] = useState(null);

    const fetchData = async () => {
        try {
            const data = await solicitudService.GetAllSolicitudes();
            setSolicitudesData(data.map(item => ({
                id: item.id,
                nombre_adoptante: item.nombre_adoptante,
                nombre_animal: item.nombre_animal,
                fecha_solicitud: item.fecha_solicitud,
                datos_adicionales: item.datos_adicionales,
                estado: item.estado,
            })));
            console.log("prueba", data);
        } catch (error) {
            setError(error.message);
        }
    };

    const columns = [
        { header: "Id", accessorKey: "id" },
        { header: "Nombre del Adoptante", accessorKey: "nombre_adoptante" },
        { header: "Nombre del Animal", accessorKey: "nombre_animal" },
        { header: "Fecha de Solicitud", accessorKey: "fecha_solicitud" },
        { header: "Datos Adicionales", accessorKey: "datos_adicionales" },
        { header: "Estado", accessorKey: "estado" },
        // Puedes agregar más columnas según sea necesario
    ];

    const inputConfig = [
        {
            name: "nombre_adoptante",
            type: "text",
            placeholder: "Nombre del Adoptante",
            validations: { required: "El nombre del adoptante es requerido" },
        },
        {
            name: "nombre_animal",
            type: "text",
            placeholder: "Nombre del Animal",
            validations: { required: "El nombre del animal es requerido" },
        },
        {
            name: "fecha_solicitud",
            type: "date",
            placeholder: "Fecha de Solicitud",
            validations: { required: "La fecha de solicitud es requerida" },
        },
        {
            name: "datos_adicionales",
            type: "text",
            placeholder: "Datos Adicionales",
            validations: { required: "Los datos adicionales son requeridos" },
        },
        {
            name: "estado",
            type: "select",
            placeholder: "Estado",
            validations: { required: "El estado es requerido" },
            options: [
                { value: "Registrado", label: "Registrado" },
                { value: "Aprobado", label: "Aprobado" },
                { value: "Denegado", label: "Denegado" },
            ],
        },
        // Puedes agregar más campos según sea necesario
    ];


    const initialValues = {
        id_del_usuario: "",
        nombre_adoptante: "",
        nombre_animal: "",
        fecha_solicitud: "",
        datos_adicionales: "",
        estado: "",
        // Puedes agregar más campos según sea necesario
    };

    const onDelete = async (id) =>{
        try{
            const data = await solicitudService.DeleteSolicitud(id);
            console.log(data)
            fetchData()
        }catch(error){
            setError(error.message)
        }
    }


    useEffect(() => {


        fetchData();
    }, []);

    const onEdit = async (rowData) => {
        // Lógica para editar la fila, por ejemplo, abrir un formulario de edición
        console.log(rowData);
        try {
            //     // Lógica para editar la fila, por ejemplo, abrir un formulario de edición
            //     // console.log("AAAAAAAAAAAAAAAAeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", rowData)
            const mappedData = {
                fecha_solicitud: rowData.fecha_solicitud,
                datos_adicionales: rowData.datos_adicionales,
                estado: rowData.estado
            };

                await solicitudService.UpdateSolicitud(rowData.id, mappedData);
                fetchData();
            //     // Invertir el valor de updateData
                // setUpdateData((prevUpdateData) => !prevUpdateData);

            //     // Lógica para editar la fila, por ejemplo, abrir un formulario de edición
            //     console.log("Editando:", mappedData);
        } catch (error) {
            console.error("Error al editar la solicitud:", error.message);
            //     // Manejo de errores, por ejemplo, mostrar un mensaje al usuario
        }
    };

    return (
        <div className="bg-all py-20 text-[#0e3d69] h-screen">
            <h1 className="text-center text-6xl font-bold pb-10">Solicitudes</h1>
            <div className="grid gap-y-4  mx-[200px]">

            </div>
            <div className="grid pt-10">
                <div className="flex flex-col items-center justify-center text-center">
                    <div className="flex items-start w-9/12 pb-2">
                        {/* <SimpleDialog buttonText="Crear">
                            <Dform initialValues={initialValues} inputs={inputConfig} onSub={onSubmit} />

                        </SimpleDialog> */}
                    </div>
                </div>
                <div className="flex flex-col items-center justify-center text-center">

                    <div className="w-9/12">
                        <UTable data={solicitudesData} columns={columns} initialValues={initialValues} inputs={inputConfig} onDel={onDelete} onEdit={onEdit} />
                    </div>
                </div>
            </div>
        </div>
    );
}

