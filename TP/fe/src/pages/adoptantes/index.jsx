import { Button, Card, CardBody, Dialog, DialogBody, DialogHeader, Typography } from "@material-tailwind/react";
import Dform from "../../components/dform/dForm";

import React, { useEffect, useState } from "react";
import UTable from "../../components/uTable/utable";
import SimpleDialog from "../../components/simpleDialog/simpleDialog";
import animalService from "../../services/animalService";
import adoptanteService from "../../services/adoptanteService";


export default function Adoptantes() {

    const initialValues = {
        "nombre": "",
        "especie": "",
        "tamano": "", // Puedes establecer el valor predeterminado aquí
        "edad": "",
        "descripcion": "",
        "image": ""/* Puedes poner aquí el valor de la imagen si es necesario */,
    };

    const inputConfig = [
        {
            name: "ci",
            type: "text",
            placeholder: "CI",
            validations: {
                required: "La cédula de identidad es requerida",
                maxLength: {
                    value: 10,
                    message: "La cédula de identidad no puede exceder los 10 caracteres",
                },
            },
        },
        {
            name: "nombre",
            type: "text",
            placeholder: "Nombre",
            validations: {
                required: "El nombre es requerido",
                maxLength: {
                    value: 80,
                    message: "El nombre no puede exceder los 80 caracteres",
                },
            },
        },
        {
            name: "apellido",
            type: "text",
            placeholder: "Apellido",
            validations: { required: "El apellido es requerido" },
        },
        {
            name: "correo_electronico",
            type: "text",
            placeholder: "Correo Electrónico",
            validations: {
                required: "El correo electrónico es requerido",
                // Puedes agregar validaciones específicas para el formato del correo electrónico si es necesario
            },
        },
        {
            name: "telefono",
            type: "text",
            placeholder: "Teléfono",
            validations: {
                required: "El teléfono es requerido",
                // Puedes agregar validaciones específicas para el formato del teléfono si es necesario
            },
        },
        {
            name: "direccion",
            type: "text",
            placeholder: "Dirección",
            validations: { required: "La dirección es requerida" },
        },
        {
            name: "ocupacion",
            type: "text",
            placeholder: "Ocupación",
            validations: { required: "La ocupación es requerida" },
        },
    ];


    // Estructurar el array 'columns'
    const columns = [
        { header: "CI", accessorKey: "ci" },
        { header: "Nombre", accessorKey: "nombre" },
        { header: "Apellido", accessorKey: "apellido" },
        { header: "Correo Electrónico", accessorKey: "correo_electronico" },
        { header: "Teléfono", accessorKey: "telefono" },
        { header: "Dirección", accessorKey: "direccion" },
        { header: "Ocupación", accessorKey: "ocupacion" },
    ];



    const [animalData, setAnimalData] = useState([]);
    const [error, setError] = useState(null);

    const onSubmit = async (data) => {
        // console.log("PRUEBA", data)
        const file = data.image[0];

        if (file) {
            const imageData = await readImageFile(file);
            data.image = imageData;
        }

        console.log(data);
        data.image = data.image === "" ? "NULL" : data.image;

        try {
            const res = await animalService.CreateAnimal(data);
            console.log(res);
            await fetchData();
        } catch (error) {
            console.error("Error al crear el animal:", error);
        }
    };

    const onEdit = async (data) => {
        console.log("PRUEBA", data)


        console.log(data);
        const mappedData = {
            ci: data.ci,
            nombre: data.nombre,
            apellido: data.apellido,
            correo_electronico: data.correo_electronico,
            telefono: data.telefono,
            direccion: data.direccion,
            ocupacion: data.ocupacion
        }


        try {
            const res = await adoptanteService.UpdateAdoptante(data.id, mappedData);
            console.log(mappedData);

            await fetchData();
        } catch (error) {
            console.error("Error al actualizar el adoptante:", error);
        }
    };

    const readImageFile = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = (e) => {
                resolve(e.target.result);
            };

            reader.onerror = (error) => {
                reject(error);
            };

            reader.readAsDataURL(file);
        });
    };

    const onDelete = async (id) => {
        try {
            const data = await adoptanteService.DeleteAdoptante(id);
            console.log(data)
            fetchData()
        } catch (error) {
            setError(error.message)
        }
    }

    const fetchData = async () => {

        try {
            const data = await adoptanteService.GetAllAdoptantes();
            setAnimalData(data.map(item => ({
                id: item.id,
                ci: item.ci,
                nombre: item.nombre,
                apellido: item.apellido,
                correo_electronico: item.correo_electronico,
                telefono: item.telefono,
                direccion: item.direccion,
                ocupacion: item.ocupacion,
            })));
            console.log("prueba", data)
        } catch (error) {
            setError(error.message);
        }
    };



    useEffect(() => {


        fetchData();
    }, []);


    return (
        <div className="bg-all py-20 text-[#0e3d69] h-screen">
            <h1 className="text-center text-6xl font-bold pb-10">Adoptantes</h1>
            <div className="grid gap-y-4  mx-[200px]">

            </div>
            <div className="grid pt-10">
                <div className="flex flex-col items-center justify-center text-center">
                    <div className="flex items-start w-9/12 pb-2">
                        {/* <SimpleDialog buttonText="Crear">
                            <Dform initialValues={initialValues} inputs={inputConfig} onSub={onSubmit} />

                        </SimpleDialog> */}
                    </div>
                </div>
                <div className="flex flex-col items-center justify-center text-center">

                    <div className="w-9/12">
                        <UTable data={animalData} columns={columns} initialValues={initialValues} inputs={inputConfig} onDel={onDelete} onEdit={onEdit} />
                    </div>
                </div>
            </div>
        </div>
    );
}

