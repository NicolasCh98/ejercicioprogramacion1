import React, { useState, useEffect } from "react";
import AnimalCard from "../../components/animalCard";
import animalService from "../../services/animalService";
import { Link } from "react-router-dom";
import { useAuth } from "../../context/authProvider";

const LoadingSection = ({ cantidad }) => {
  // Crea un array con la cantidad especificada
  const bloques = Array.from({ length: cantidad });

  return (
    <>
    {bloques.map((_, index) => (
      <div key={index} className="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <div className="animate-pulse rounded-t-lg w-full h-[255px] bg-gray-300 dark:bg-gray-600"></div>
        <div className="p-5">
          <h5 className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
            <span className="w-3/4 h-6 bg-gray-200 dark:bg-gray-700 block"></span>
          </h5>
          <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
            <span className="w-full h-4 bg-gray-200 dark:bg-gray-700 block"></span>
            <span className="w-5/6 h-4 bg-gray-200 dark:bg-gray-700 block mt-2"></span>
          </p>

        </div>

      </div>
      ))}
    </>

  );
};


export default function Animales() {
  const { animalCount } = useAuth();
  const [animalData, setAnimalData] = useState([]);
  const [animales, setAnimales] = useState([]);
  const [cantRegistros, setCantRegistros] = useState(animalCount);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const fetchAnimales = async () => {
      try {
        // Llamar al servicio para obtener los animales
        console.log(animalCount)
        const animalesData = await animalService.GetAllAnimals();
        setAnimalData(animalesData);
        console.log(cantRegistros)
        console.log(animalData)
        // setAnimales(mappedData(animalData))
      } catch (error) {
        console.error("Error al obtener animales:", error);
      }
    };

    // Llamar a la función de obtención de datos al montar el componente
    fetchAnimales();
  }, []);

  useEffect(() => {
    // Actualizar los animales después de que animalData se haya actualizado
    setAnimales(mappedData(animalData));
    setTimeout(() => {
      setIsLoading(false)

    }, 2000)
  }, [animalData]);

  const mappedData = (data) => {
    // console.log(data)
    const datMap = data.map(animal => ({
      id: animal.id,
      imagen: animal.imagen_path, // Puedes ajustar esto según la lógica necesaria
      nombre: animal.nombre,
      descripcion: animal.descripcion,
      slug: animal.nombre.toLowerCase().replace(/\s+/g, '-'), // Convierte el nombre a minúsculas y reemplaza espacios con guiones
    }))
    return datMap
  }


  // const animales = [
  //   {
  //     imagen: "/src/assets/perro-ex-1.jpg",
  //     nombre: "Frodito",
  //     descripcion:
  //       "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
  //     slug: "frodito",
  //   },
  //   {
  //     imagen: "/src/assets/perro-ex-2.jpg",
  //     nombre: "Pompita",
  //     descripcion:
  //       "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
  //     slug: "pompita",
  //   },
  //   {
  //     imagen: "/src/assets/perro-ex-3.jpg",
  //     nombre: "Negri",
  //     descripcion:
  //       "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
  //     slug: "negri",
  //   },
  //   ,
  //   {
  //     imagen: "/src/assets/perro-ex-3.jpg",
  //     nombre: "Blanqui",
  //     descripcion:
  //       "Here are the biggest enterprise technology acquisitions of 2021 so far, in reverse chronological order.",
  //     slug: "blanqui",
  //   },
  // ];

  return (
    <>
      <div className="bg-[#143f6e] h-screen flex justify-center py-[30px]">
        <div className="container text-white mx-[100px]">
          <h1 className="text-7xl font-bold text-center pb-[30px]">Animales</h1>
          {isLoading ? (
            <div className="grid grid-cols-3 auto-rows-auto gap-5">
              <LoadingSection cantidad={cantRegistros} />
            </div>
          ) : (
            <div className="grid grid-cols-3 auto-rows-auto gap-5">
              {animales.map((animal: any, index: number) => (
                <AnimalCard
                  id={animal.id}
                  key={index}
                  imagen={animal.imagen}
                  nombre={animal.nombre}
                  descripcion={animal.descripcion}
                  slug={animal.slug}
                  texto={animal.descripcion}
                />
              ))}
            </div>
          )}
        </div>
      </div>

    </>
  );
}
