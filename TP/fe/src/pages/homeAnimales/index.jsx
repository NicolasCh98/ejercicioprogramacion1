import { Button, Card, CardBody, Dialog, DialogBody, DialogHeader, Typography } from "@material-tailwind/react";
import Dform from "../../components/dform/dForm";

import React, { useEffect, useState } from "react";
import UTable from "../../components/uTable/utable";
import SimpleDialog from "../../components/simpleDialog/simpleDialog";
import animalService from "../../services/animalService";


export default function HomeAnimales() {

    const initialValues = {
        "nombre": "",
        "especie": "",
        "tamano": "", // Puedes establecer el valor predeterminado aquí
        "edad": "",
        "descripcion": "",
        "image": ""/* Puedes poner aquí el valor de la imagen si es necesario */,
    };

    const inputConfig = [
        {
            name: "nombre",
            type: "text",
            placeholder: "Nombre",
            validations: {
                required: "El nombre es requerido",
                maxLength: {
                    value: 80,
                    message: "El nombre no puede exceder los 80 caracteres",
                },
            },
        },
        {
            name: "especie",
            type: "text",
            placeholder: "Especie",
            validations: { required: "La especie es requerida" },
        },
        {
            name: "tamano",
            type: "select",
            placeholder: "Tamaño",
            validations: { required: "El tamaño es requerido" },
            options: [
                { value: "pequenho", label: "Pequeño" },
                { value: "mediano", label: "Mediano" },
                { value: "grande", label: "Grande" },
            ],
        },
        {
            name: "edad",
            type: "number",
            placeholder: "Edad",
            validations: { required: "La edad es requerida" },
        },
        {
            name: "descripcion",
            type: "text",
            placeholder: "Descripción",
            validations: { required: "La descripción es requerida" },
        },
        {
            name: "image",
            type: "file",
            placeholder: "Imagen",
            validations: {  },
        },
    ];

    // Estructurar el array 'columns'
    const columns = [
        { header: "Id", accessorKey: "id" },
        { header: "Nombre", accessorKey: "nombre" },
        { header: "Especie", accessorKey: "especie" },
        { header: "Tamaño", accessorKey: "tamano" },
        { header: "Edad", accessorKey: "edad" },
        { header: "Descripción", accessorKey: "descripcion" },
        // Incluir la columna de imagen con un render personalizado si es necesario
        { header: "Imagen", accessorKey: "image", render: (row) => <img src={row.image} alt="Animal" style={{ width: "50px" }} /> },
    ];


    const [animalData, setAnimalData] = useState([]);
    const [error, setError] = useState(null);

    const onSubmit = async (data) => {
        // console.log("PRUEBA", data)
        const file = data.image[0];
        
        if (file) {
            const imageData = await readImageFile(file);
            data.image = imageData;
        }
    
        console.log(data);
        data.image = data.image === "" ? "NULL": data.image;
    
        try {
            const res = await animalService.CreateAnimal(data);
            console.log(res);
            await fetchData();
        } catch (error) {
            console.error("Error al crear el animal:", error);
        }
    };

    const onEdit = async (data) => {
        console.log("PRUEBA", data)
        const file = data.image[0];
        
        if (file) {
            const imageData = await readImageFile(file);
            data.image = imageData;
        }
    
        console.log(data);
    
        try {
            const res = await animalService.EditAnimal(data.id, data);
            console.log(res);
            await fetchData();
        } catch (error) {
            console.error("Error al crear el animal:", error);
        }
    };
    
    const readImageFile = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
    
            reader.onload = (e) => {
                resolve(e.target.result);
            };
    
            reader.onerror = (error) => {
                reject(error);
            };
    
            reader.readAsDataURL(file);
        });
    };

    const onDelete = async (id) =>{
        try{
            const data = await animalService.DeleteAnimal(id);
            console.log(data)
            fetchData()
        }catch(error){
            setError(error.message)
        }
    }

    const fetchData = async () => {
        
        try {
            const data = await animalService.GetAllAnimals();
            setAnimalData(data.map(item => ({
                id: item.id,
                nombre: item.nombre,
                especie: item.especie,
                tamano: item.tamano,
                edad: item.edad,
                descripcion: item.descripcion,
                // Usar la URL completa de la imagen
                image: item.imagen_path,
            })));
            console.log("prueba", data)
        } catch (error) {
            setError(error.message);
        }
    };



    useEffect(() => {
        

        fetchData();
    }, []);


    return (
        <div className="bg-all py-20 text-[#0e3d69] h-screen">
            <h1 className="text-center text-6xl font-bold pb-10">Animales</h1>
            <div className="grid gap-y-4  mx-[200px]">

            </div>
            <div className="grid pt-10">
                <div className="flex flex-col items-center justify-center text-center">
                    <div className="flex items-start w-9/12 pb-2">
                        <SimpleDialog buttonText="Crear">
                            <Dform initialValues={initialValues} inputs={inputConfig} onSub={onSubmit} />

                        </SimpleDialog>
                    </div>
                </div>
                <div className="flex flex-col items-center justify-center text-center">

                    <div className="w-9/12">
                        <UTable data={animalData} columns={columns} initialValues={initialValues} inputs={inputConfig} onDel={onDelete} onEdit={onEdit}/>
                    </div>
                </div>
            </div>
        </div>
    );
}

