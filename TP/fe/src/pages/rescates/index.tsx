import React, { useEffect, useState } from "react";
// import { Card, IconButton, Typography } from "@material-tailwind/react";
import withMT from "@material-tailwind/react/utils/withMT";
import animalService from "../../services/animalService";
import rescateService from "../../services/rescateService";
import loginService from "../../services/loginService";

import { useAuth } from "../../context/authProvider";
import SimpleDialog from "../../components/simpleDialog/simpleDialog";
import Dform from "../../components/dform/dForm";
import UTable from "../../components/uTable/utable";


export default function Rescates() {
    const context = useAuth();
    console.log(context);
    const [exampleData, setExampleData] = useState([]);
    const [updateData, setUpdateData] = useState(false);
    const [error, setError] = useState(null);

    
    const onDelete = async (id) => {
        if (id != null) {
            try {
                await rescateService.deleteRescate(id);
                setUpdateData((prevUpdateData) => !prevUpdateData);
                setExampleData((prevData) => prevData.filter((item) => item.id !== id));
            } catch (error) {
                console.error("Error deleting data:", error);
                setError(error.message)
            }
        }
    }


    const handleDelete = async (id) => {
        if (id != null) {
            try {
                await rescateService.deleteRescate(id);
                setUpdateData((prevUpdateData) => !prevUpdateData);
                setExampleData((prevData) => prevData.filter((item) => item.id !== id));
            } catch (error) {
                console.error("Error deleting data:", error);
            }
        }
    };



    useEffect(() => {
        console.log("IDDDDDDDDD", context)
        const fetchAndMapData = async () => {
            try {
                const info = await rescateService.GetAllRescates();
                const mappedInfo = info.map(rescate => ({
                    ...rescate,
                    informacion_adicional_rescate: rescate.informacion_adicional,
                }));
                console.log("Datos", mappedInfo);
                setExampleData(mappedInfo); // Set the state with the mapped data
            } catch (error) {
                console.error("Error fetching data:", error);
                // Handle the error as needed
                setExampleData([]); // Return an empty array or handle the error accordingly
            }
        };

        fetchAndMapData(); // Call the function when the component mounts
    }, [updateData]);


    const handleEdit = async (rowData) => {
        // Lógica para editar la fila, por ejemplo, abrir un formulario de edición
        try {
            // Lógica para editar la fila, por ejemplo, abrir un formulario de edición
            // console.log("AAAAAAAAAAAAAAAAeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee", rowData)
            const mappedData = {
                estado: rowData.estado,
                direccion: rowData.direccion,
                fecha_rescate: rowData.fecha_rescate,
                informacion_adicional: rowData.informacion_adicional_rescate,
            };
    
            await rescateService.updateRescate(rowData.id, mappedData);
    
            // Invertir el valor de updateData
            setUpdateData((prevUpdateData) => !prevUpdateData);
    
            // Lógica para editar la fila, por ejemplo, abrir un formulario de edición
            console.log("Editando:", mappedData);
        } catch (error) {
            console.error("Error al editar el rescate:", error.message);
            // Manejo de errores, por ejemplo, mostrar un mensaje al usuario
        }
    };

    const exampleColumns = ["Nombre", "Especie", "Tamaño", "Edad", "Fecha"];

    const initialValues = {
        "id_usuario": "",
        "direccion": "",
        "estado": "",
        "fecha_rescate": "",
        "informacion_adicional_rescate": "",
        "id_animal_nombre": "hola",
        "id_animal_especie": "",
        "id_animal_tamano": "mediano", // Puedes establecer el valor predeterminado aquí
        "id_animal_edad": "",
        "id_animal_descripcion": "",
        "id_animal_image": "" /* Puedes poner aquí el valor de la imagen si es necesario */,
    };

    const initialValues2 = {
        "id_usuario": "",
        "direccion": "",
        "estado": "",
        "fecha_rescate": "",
        "informacion_adicional_rescate": "",
    };

   
    const inputConfig2 = [
        {
            name: "direccion",
            type: "text",
            placeholder: "Dirección del rescate",
            validations: { required: "La dirección del rescate es requerida" },
        },
        {
            name: "fecha_rescate",
            type: "date",
            placeholder: "Fecha de rescate",
            validations: { required: "La fecha de rescate es requerida" },
        },
        {
            name: "estado",
            type: "select",
            placeholder: "Estado",
            validations: { required: "El estado es requerido" },
            options: [
                { value: "Registrado", label: "Registrado" },
                { value: "Rescatado", label: "Rescatado" },
                { value: "Cancelado", label: "Cancelado" },
            ],
        },
        {
            name: "informacion_adicional_rescate",
            type: "text",
            placeholder: "Información adicional del rescate",
            validations: {},
        },
    
    ];


    const inputConfig = [
        {
            name: "direccion",
            type: "text",
            placeholder: "Dirección del rescate",
            validations: { required: "La dirección del rescate es requerida" },
        },
        {
            name: "fecha_rescate",
            type: "date",
            placeholder: "Fecha de rescate",
            validations: { required: "La fecha de rescate es requerida" },
        },
        {
            name: "informacion_adicional_rescate",
            type: "text",
            placeholder: "Información adicional del rescate",
            validations: {},
        },
        {
            name: "id_animal_nombre",
            type: "text",
            placeholder: "Nombre del animal",
            validations: {
                required: "El nombre es requerido",
                maxLength: {
                    value: 80,
                    message: "El nombre no puede exceder los 80 caracteres",
                },
            },
        },
        {
            name: "id_animal_especie",
            type: "text",
            placeholder: "Especie",
            validations: { required: "La especie es requerida" },
        },
        {
            name: "id_animal_tamano",
            type: "select",
            placeholder: "Tamaño",
            validations: { required: "El tamaño es requerido" },
            options: [
                { value: "pequenho", label: "Pequeño" },
                { value: "mediano", label: "Mediano" },
                { value: "grande", label: "Grande" },
            ],
        },
        {
            name: "id_animal_edad",
            type: "number",
            placeholder: "Edad",
            validations: { required: "La edad es requerida" },
        },
        {
            name: "id_animal_descripcion",
            type: "text",
            placeholder: "Descripción",
            validations: { required: "La descripción es requerida" },
        },
        {
            name: "id_animal_image",
            type: "file",
            placeholder: "Imagen",
            validations: {},
        },
    ];


    const onSubmit = async (data) => {
        // console.log("PRUEBA", data)
        const file = data.id_animal_image[0];
        const user = await loginService.GetUser(context.token);
        console.log(user.user.id);
        if (file) {
            const imageData = await readImageFile(file);
            data.image = imageData;
        }

        console.log(data);
        const mappedData = {
            id_usuario: user.user.id || null, // Asigna el valor de id_usuario o null si no está presente
            direccion: data.direccion || "", // Asigna el valor de direccion o una cadena vacía si no está presente
            estado: data.estado || "Registrado", // Asigna el valor de estado o una cadena vacía si no está presente
            fecha_rescate: data.fecha_rescate || "", // Asigna el valor de fecha_rescate o una cadena vacía si no está presente
            informacion_adicional: data.informacion_adicional_rescate || "", // Asigna el valor de informacion_adicional_rescate o una cadena vacía si no está presente
            id_animal: {
                nombre: data.id_animal_nombre || "", // Asigna el valor de id_animal_nombre o una cadena vacía si no está presente
                especie: data.id_animal_especie || "", // Asigna el valor de id_animal_especie o una cadena vacía si no está presente
                tamano: data.id_animal_tamano || "", // Asigna el valor de id_animal_tamano o una cadena vacía si no está presente
                edad: parseInt(data.id_animal_edad) || 0, // Convierte el valor de id_animal_edad a entero o asigna 0 si no es un número
                descripcion: data.id_animal_descripcion || "", // Asigna el valor de id_animal_descripcion o una cadena vacía si no está presente
                image: data.image || "", // Asigna el valor de la imagen procesada o una cadena vacía si no está presente
            },
        };
    
        console.log(mappedData);

        try {
            // const res = await animalService.CreateAnimal(data);
            await rescateService.CreateRescate(mappedData);
            console.log("");
            setUpdateData((prevUpdateData) => !prevUpdateData)
            // await fetchData();
        } catch (error) {
            console.error("Error al crear el rescate:", error);
        }
    };



    const readImageFile = (file) => {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();

            reader.onload = (e) => {
                resolve(e.target.result);
            };

            reader.onerror = (error) => {
                reject(error);
            };

            reader.readAsDataURL(file);
        });
    };


    // const formFields = [
    //     // { label: "Estado", type: "text" },
    //     { label: "Nombre", type: "text" },
    //     { label: "Especie", type: "text" },
    //     {
    //         label: "Tamaño",
    //         type: "select",
    //         options: [
    //             {
    //                 id: 1,
    //                 text: "Pequeño",
    //             },
    //             {
    //                 id: 2,
    //                 text: "Mediano",
    //             },
    //             {
    //                 id: 3,
    //                 text: "Grande",
    //             },
    //         ],
    //     },
    //     { label: "Edad", type: "text" },
    //     { label: "Fecha_Rescate", type: "date" },
    //     { label: "Informacion_Adicional", type: "text" },

    // ];

    //crear rescate
    const handleSubmit = async (data) => {
        try {
            const user = await loginService.GetUser(context.token);
            console.log(user.user);
            const formattedData = {
                id_usuario: user.user.id,
                direccion: data.Direccion || "Dirección del rescate",
                estado: data.Estado || "En curso",
                fecha_rescate: data.Fecha_Rescate || "2023-11-19",
                informacion_adicional: data.Informacion_Adicional || "string",
                id_animal: {
                    nombre: data.Nombre || "Nombre del animal",
                    especie: data.Especie || "Especie del animal",
                    tamano: data.Tamaño || "tamano del animal",
                    edad: data.Edad || 3,
                    descripcion: data.Descripcion || "Descripción del animal",
                },
            };

            console.log(data);

            // Hacer la llamada a la función asincrónica
            await rescateService.CreateRescate(formattedData);

            // Actualizar el estado para activar el useEffect
            setUpdateData((prevUpdateData) => !prevUpdateData);
        } catch (error) {
            console.error("Error fetching user data:", error);
            // Handle the error as needed
        }
    };

    const columns = [
        { header: "Id", accessorKey: "id" },
        { header: "Estado", accessorKey: "estado" },
        { header: "Dirección", accessorKey: "direccion" },
        { header: "Fecha de Rescate", accessorKey: "fecha_rescate" },
        { header: "Información Adicional", accessorKey: "informacion_adicional" },
        {
            header: "Animal",
            accessorKey: "animal.nombre"
        },
    ];


    return (
        <div className="bg-all py-20 text-[#0e3d69] h-screen">
            <h1 className="text-center text-6xl font-bold pb-10">Rescates</h1>
            <div className="grid gap-y-4  mx-[200px]">

            </div>
            <div className="grid pt-10">
                <div className="flex flex-col items-center justify-center text-center">
                    <div className="flex items-start w-9/12 pb-2">
                        <SimpleDialog buttonText="Crear">
                            <Dform initialValues={initialValues} inputs={inputConfig} onSub={onSubmit} />

                        </SimpleDialog>
                    </div>
                </div>
                <div className="flex flex-col items-center justify-center text-center">

                    <div className="w-9/12">
                        <UTable data={exampleData} columns={columns} initialValues={initialValues2} inputs={inputConfig2} onDel={onDelete} onEdit={handleEdit} />
                    </div>
                </div>
            </div>
        </div>
        // <div className="bg-all py-20 text-[#0e3d69] h-screen">
        //     <h1 className="text-center text-6xl font-bold pb-10">Rescates</h1>
        //     <div className="grid gap-y-4  mx-[200px]">
        //         {/* <TabComponent /> */}
        //         <DynamicFormDialogg
        //             fields={formFields}
        //             onSubmit={handleSubmit}
        //             titulo={"Nuevo Rescate"}
        //         />

        //         <DynamicTable
        //             formFields={formFields}
        //             data={exampleData}
        //             selectedColumns={selectedColumns}
        //             // columns={exampleColumns}
        //             handleDelete={handleDelete}
        //             onEdit={handleEdit}
        //         />
        //     </div>
        //     <div className="grid pt-10 mx-[200px]">
        //         <div className="flex flex-col items-center justify-center text-center">
        //             <div>

        //             </div>
        //         </div>
        //     </div>
        // </div>
    );
}