import { useContext } from 'react';
import { createContext } from "react";
import { useEffect } from 'react';
import { useState } from 'react';
import animalService from '../services/animalService';

export const authContext = createContext();

export const useAuth = () => {
    const context = useContext(authContext);
    return context
}

// eslint-disable-next-line react/prop-types
const AuthProvider = ({ children }) => {

    const [token, setToken] = useState(null);

    const [currentUser, setCurrentUser] = useState(null);

    const [isLoading, setIsLoading] = useState(false);

    const [animalCount, setAnimalCount] = useState(1);

    // const fetchData = async () => {

    //     try {
    //         const data = await animalService.GetCountAnimals();
    //         setAnimalCount(data.count)
    //         // setAnimalData(data.map(item => ({
    //         //     value: item.id,
    //         //     label: item.nombre,
    //         // })));
    //         console.log("prueba", data)
    //     } catch (error) {
    //         // setError(error.message);
    //         console.log(error)
    //     }
    // };


    useEffect(() => {
        const fetchData = async () => {
            try {
                const data = await animalService.GetCountAnimals();
                setAnimalCount(data.count);
            } catch (error) {
                console.log(error);
            }
        };

        fetchData(); // Llamar a fetchData inmediatamente después del montaje del componente

        // Resto del código...
    }, []);

    if(animalCount === 1){
        const LazyLoading = () => {
            return <div className="loader">
                <i className="fa-solid fa-paw paw-1"></i>
                <i className="fa-solid fa-paw paw-2"></i>
                <i className="fa-solid fa-paw paw-3"></i>
            </div>
        }
        return (
            <LazyLoading/>
        )
    }

    return (

        <authContext.Provider value={{ currentUser, setCurrentUser, isLoading, setIsLoading, token, setToken, animalCount }}>
            
            {children}
        </authContext.Provider>
    )
}

export default AuthProvider