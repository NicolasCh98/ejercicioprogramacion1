import { useState } from "react"
import { useEffect } from "react"
import { Navigate, useNavigate } from "react-router-dom"
import { useAuth } from "../context/authProvider"
import { useGlobalData } from "../context/dataContext"


const LazyLoading = () => {
    return <div className="loader">
        <i className="fa-solid fa-paw paw-1"></i>
        <i className="fa-solid fa-paw paw-2"></i>
        <i className="fa-solid fa-paw paw-3"></i>
    </div>
}

export const ProtectedRoute = ({ children }) => {
    const context = useAuth();
    const { loadingData } = useGlobalData();
    const [load, setLoad] = useState(true);

    const navigate = useNavigate();

    // Función para decodificar el token y obtener la fecha de expiración

    const getCookie = (name) => {
        const cookies = document.cookie.split(';');
        for (const cookie of cookies) {
            const [cookieName, cookieValue] = cookie.split('=');
            if (cookieName.trim() === name) {
                return cookieValue;
            }
        }
        return null;
    };

    // Redirige a la ruta principal si no está autenticado
    // if (loadingUser) return <LazyLoading />;
    // useEffect(() => {
    //     const setTokenFromCookie = () => {
    //         try {
    //             const cookie = getCookie("token");
    //             if (cookie) {
    //                 context.setToken(cookie);
    //             } else {
    //                 // Puede ser útil redirigir a la página de inicio de sesión si no hay token
    //                 navigate('/', { replace: true });
    //             }
    //         } catch (error) {
    //             console.error("Error al procesar la cookie:", error);
    //             // Manejar el error, por ejemplo, redirigir a una página de error
    //         }
    //     };
    //     console.log("HOLAAAA", context.currentUser)
    //     setTokenFromCookie();
    // }, [context]);
    useEffect(() => {
        const setTokenFromCookie = () => {
          try {
            const cookie = getCookie("token");
            if (cookie) {
              context.setToken(cookie);
      
              // Recupera información del usuario si es necesario y actualiza el estado
              // Puedes llamar a una función que haga una petición para obtener la información del usuario
              // y luego actualizar el estado con setCurrentUser.
            } else {
              // Puede ser útil redirigir a la página de inicio de sesión si no hay token
              navigate('/', { replace: true });
            }
          } catch (error) {
            console.error("Error al procesar la cookie:", error);
            // Manejar el error, por ejemplo, redirigir a una página de error
          }
        };
      
        setTokenFromCookie();
      }, [context, navigate]);


    // if (!currentUserr) {
    //     navigate('/', { replace: true }); // Usamos navigate para redirigir programáticamente
    //     return null; // No renderizamos nada mientras se redirige
    // }

    // Si está autenticado, renderiza los hijos
    return (
        <>
            {context.isLoading === true ? <LazyLoading /> :  children }
            {console.log(context)}
            {/* {children} */}
            {/* <LazyLoading/> */}
        </>
    );
}
