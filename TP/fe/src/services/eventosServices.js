import axios from "axios";

const url  = "https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/eventos";

// const CreateRescate = (newRescate, {token}) => {
//     const config = {
//         headers: {
//             Authorization: Bearer ${token} 
//         }
//     }
//     const request = axios.post(url, newRescate, config);
//     return request.then(response => response.data);
// }

const GetAllEventos = () => {
    const request = axios.get(url);
    return request.then(response => response.data)
}

const CreateEvento = (newEvento) => {
    const request = axios.post(url, newEvento);
    return request.then(response => response.data);
}

const deleteEvento = (id) => {
    const deleteUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/eventos/${id}`;

    return axios.delete(deleteUrl)
        .then(response => {
            // Si la solicitud se completó con éxito (código 204), no habrá datos en la respuesta.
            if (response.status === 204) {
                return { success: true, message: 'Evento eliminado exitosamente' };
            } else {
                // Si la API devuelve otro código de estado, maneja la respuesta aquí
                return { success: false, message: 'No se pudo eliminar el evento' };
            }
        })
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return { success: false, message: 'Evento no encontrado' };
            } else {
                return { success: false, message: 'Error al procesar la solicitud' };
            }
        });
};

const updateEvento = (id, updatedRescate) => {
    const updateUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/eventos/${id}`;

    const request = axios.put(updateUrl, updatedRescate);
    
    return request.then(response => response.data)
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return { success: false, message: 'Evento no encontrado' };
            } else {
                return { success: false, message: 'Error al procesar la solicitud' };
            }
        });
};

export default { GetAllEventos, CreateEvento, deleteEvento, updateEvento };


// export default { GetAllRescates, CreateRescate }