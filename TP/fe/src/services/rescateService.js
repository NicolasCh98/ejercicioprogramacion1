import axios from "axios";

const url  = "https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/rescatesA";
const getRescatesUrl = "https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/rescates";

// const CreateRescate = (newRescate, {token}) => {
//     const config = {
//         headers: {
//             Authorization: Bearer ${token} 
//         }
//     }
//     const request = axios.post(url, newRescate, config);
//     return request.then(response => response.data);
// }

const GetAllRescates = () => {
    const request = axios.get(getRescatesUrl);
    return request.then(response => response.data)
}

const CreateRescate = (newRescate) => {
    const request = axios.post(url, newRescate);
    return request.then(response => response.data);
}

const deleteRescate = (id) => {
    const deleteUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/rescates/${id}`;

    return axios.delete(deleteUrl)
        .then(response => {
            // Si la solicitud se completó con éxito (código 204), no habrá datos en la respuesta.
            if (response.status === 204) {
                return { success: true, message: 'Rescate eliminado exitosamente' };
            } else {
                // Si la API devuelve otro código de estado, maneja la respuesta aquí
                return { success: false, message: 'No se pudo eliminar el rescate' };
            }
        })
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return { success: false, message: 'Rescate no encontrado' };
            } else {
                return { success: false, message: 'Error al procesar la solicitud' };
            }
        });
};

const updateRescate = (id, updatedRescate) => {
    const updateUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/rescates/${id}`;

    const request = axios.put(updateUrl, updatedRescate);
    
    return request.then(response => response.data)
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return { success: false, message: 'Rescate no encontrado' };
            } else {
                return { success: false, message: 'Error al procesar la solicitud' };
            }
        });
};

export default { GetAllRescates, CreateRescate, deleteRescate, updateRescate };


// export default { GetAllRescates, CreateRescate }