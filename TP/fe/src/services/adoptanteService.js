import axios from "axios";

const adoptanteUrl = 'https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/adoptantes';

const GetAllAdoptantes = () => {
    const request = axios.get(adoptanteUrl);
    return request.then(response => response.data)
}


const CreateAdoptante = (newAdoptante) => {
    return axios.post(adoptanteUrl, newAdoptante)
      .then(response => response.data)
      .catch(error => {
        if (error.response && error.response.status === 422) {
          // Si la respuesta es un error 422, obtenemos el mensaje de error del cuerpo de la respuesta
          throw new Error(error.response.data.error);
        } else {
          // Manejar otros errores de red u otros errores HTTP aquí
          throw error;
        }
      });
  };

const DeleteAdoptante = (id) => {
    const adoptanteUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/adoptantes/${id}`;

    return axios.delete(adoptanteUrl)
        .then(response => {
            // Si la solicitud se completó con éxito (código 204), no habrá datos en la respuesta.
            console.log(response.status)
            if (response.status === 204) {
                return { success: true, message: 'Adoptante eliminado exitosamente' };
            } else {
                // Si la API devuelve otro código de estado, maneja la respuesta aquí
                return { success: false, message: 'No se pudo eliminar el Adoptante' };
            }
        })
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return { success: false, message: 'Adoptante no encontrado' };
            } else {
                return { success: false, message: 'Error al procesar la solicitud' };
            }
        });
};

const UpdateAdoptante = (id, updatedAdoptante) => {
    const adoptanteUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/adoptantes/${id}`;

    return axios.put(adoptanteUrl, updatedAdoptante)
        .then(response => response.data)
        .catch(error => {
            if (error.response && error.response.status === 422) {
                // Si la respuesta es un error 422, obtenemos el mensaje de error del cuerpo de la respuesta
                throw new Error(error.response.data.error);
            } else {
                // Manejar otros errores de red u otros errores HTTP aquí
                throw error;
            }
        });
};


export default { GetAllAdoptantes, CreateAdoptante, DeleteAdoptante, UpdateAdoptante }