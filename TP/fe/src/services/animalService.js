import axios from "axios";

const animalUrl = 'https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/animales';

const GetAllAnimals = () => {
    const request = axios.get(animalUrl);
    return request.then(response => response.data)
}

const GetCountAnimals = () => {
    const countUrl = 'https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/animales/count';
    const request = axios.get(countUrl);
    return request.then(response => response.data)
}

// const CreateAnimal = (newAnimal, {token}) => {
const CreateAnimal = (newAnimal) => {
    // const config = {
    //     headers: {
    //         Authorization: `Bearer ${token}` 
    //     }
    // }
    // const request = axios.post(animalUrl, newAnimal, config);
    const request = axios.post(animalUrl, newAnimal);
    return request.then(response => response.data);
}

const DeleteAnimal = (id) => {
    const deleteUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/animales/${id}`;

    return axios.delete(deleteUrl)
        .then(response => {
            // Si la solicitud se completó con éxito (código 204), no habrá datos en la respuesta.
            console.log(response.status)
            if (response.status === 200) {
                return { success: true, message: 'Animal eliminado exitosamente' };
            } else {
                // Si la API devuelve otro código de estado, maneja la respuesta aquí
                return { success: false, message: 'No se pudo eliminar el Animal' };
            }
        })
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return { success: false, message: 'Animal no encontrado' };
            } else {
                return { success: false, message: 'Error al procesar la solicitud' };
            }
        });
};

const EditAnimal = (id, updatedAnimal) => {
    const editUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/animales/${id}`;
    
    // Puedes incluir el token de autorización si es necesario
    // const config = {
    //     headers: {
    //         Authorization: `Bearer ${token}` 
    //     }
    // }

    const request = axios.put(editUrl, updatedAnimal);
    // Puedes incluir el objeto `config` como segundo parámetro si es necesario

    return request.then(response => response.data);
}


export default { GetAllAnimals, CreateAnimal, DeleteAnimal, GetCountAnimals, EditAnimal }