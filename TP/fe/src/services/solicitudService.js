import axios from "axios";

// const url  = "https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/rescatesA";
const urlSolicitudes = "https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/solicitudes";

const GetAllSolicitudes = () => {
    const request = axios.get(urlSolicitudes);
    return request.then(response => response.data)
}

const CreateSolicitud = (newSolicitud) => {
    const request = axios.post(urlSolicitudes, newSolicitud);
    return request.then(response => response.data);
}

const DeleteSolicitud = (id) => {
    const deleteUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/solicitudes/${id}`;

    return axios.delete(deleteUrl)
        .then(response => {
            // Si la solicitud se completó con éxito (código 204), no habrá datos en la respuesta.
            if (response.status === 204) {
                return { success: true, message: 'Solicitud eliminada exitosamente' };
            } else {
                // Si la API devuelve otro código de estado, maneja la respuesta aquí
                return { success: false, message: 'No se pudo eliminar la solicitud' };
            }
        })
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return { success: false, message: 'Solicitud no encontrada' };
            } else {
                return { success: false, message: 'Error al procesar la solicitud' };
            }
        });
};

const UpdateSolicitud = (id, updatedSolicitud) => {
    const updateUrl = `https://backend-padrinos-2fb017f99f7a.herokuapp.com/api/solicitudes/${id}`;

    const request = axios.put(updateUrl, updatedSolicitud);

    return request.then(response => response.data)
        .catch(error => {
            // Manejo de errores
            if (error.response && error.response.status === 404) {
                return Promise.reject({ success: false, message: 'Solicitud no encontrada' });
            } else {
                return Promise.reject({ success: false, message: 'Error al procesar la solicitud' });
            }
        });
};

export default { GetAllSolicitudes, CreateSolicitud, DeleteSolicitud, UpdateSolicitud };


// export default { GetAllRescates, CreateRescate }