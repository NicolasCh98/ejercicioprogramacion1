import { Button, IconButton, Input, Typography } from "@material-tailwind/react";
import { flexRender, getCoreRowModel, useReactTable, getPaginationRowModel, getFilteredRowModel } from "@tanstack/react-table";
import { useState } from "react";
import SimpleDialog from "../simpleDialog/simpleDialog";
import Dform from "../dform/dForm";

// const data = [
//     {
//         "id": 1,
//         "nombre": "Nombre del animal",
//         "especie": "Especie del animal",
//         "tamano": "2",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-27T01:34:38.000000Z",
//         "updated_at": "2023-11-27T01:34:38.000000Z"
//     },
//     {
//         "id": 2,
//         "nombre": "marciancio",
//         "especie": "perro",
//         "tamano": "1",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-27T01:48:30.000000Z",
//         "updated_at": "2023-11-27T01:48:30.000000Z"
//     },
//     {
//         "id": 3,
//         "nombre": "marciancio",
//         "especie": "perro",
//         "tamano": "1",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-27T01:48:31.000000Z",
//         "updated_at": "2023-11-27T01:48:31.000000Z"
//     },
//     {
//         "id": 4,
//         "nombre": "marciancio",
//         "especie": "perro",
//         "tamano": "1",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-27T01:48:37.000000Z",
//         "updated_at": "2023-11-27T01:48:37.000000Z"
//     },
//     {
//         "id": 5,
//         "nombre": "menduncan",
//         "especie": "perro",
//         "tamano": "1",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-27T01:50:59.000000Z",
//         "updated_at": "2023-11-27T01:50:59.000000Z"
//     },
//     {
//         "id": 6,
//         "nombre": "Chamaco",
//         "especie": "Gato",
//         "tamano": "3",
//         "edad": 1,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-27T02:26:22.000000Z",
//         "updated_at": "2023-11-27T02:26:22.000000Z"
//     },
//     {
//         "id": 7,
//         "nombre": "Cachorrao",
//         "especie": "Perro",
//         "tamano": "1",
//         "edad": 2,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-27T23:23:26.000000Z",
//         "updated_at": "2023-11-27T23:23:26.000000Z"
//     },
//     {
//         "id": 8,
//         "nombre": "Peludito",
//         "especie": "Gato",
//         "tamano": "2",
//         "edad": 4,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T09:15:42.000000Z",
//         "updated_at": "2023-11-28T09:15:42.000000Z"
//     },
//     {
//         "id": 9,
//         "nombre": "Rocky",
//         "especie": "Perro",
//         "tamano": "3",
//         "edad": 5,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T10:22:18.000000Z",
//         "updated_at": "2023-11-28T10:22:18.000000Z"
//     },
//     {
//         "id": 10,
//         "nombre": "Whiskers",
//         "especie": "Gato",
//         "tamano": "1",
//         "edad": 2,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T12:45:30.000000Z",
//         "updated_at": "2023-11-28T12:45:30.000000Z"
//     },
//     {
//         "id": 11,
//         "nombre": "Fluffy",
//         "especie": "Conejo",
//         "tamano": "1",
//         "edad": 1,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T14:30:15.000000Z",
//         "updated_at": "2023-11-28T14:30:15.000000Z"
//     },
//     {
//         "id": 12,
//         "nombre": "Spike",
//         "especie": "Iguana",
//         "tamano": "2",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T16:12:50.000000Z",
//         "updated_at": "2023-11-28T16:12:50.000000Z"
//     },
//     {
//         "id": 13,
//         "nombre": "Nibbles",
//         "especie": "Hámster",
//         "tamano": "1",
//         "edad": 1,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T18:01:03.000000Z",
//         "updated_at": "2023-11-28T18:01:03.000000Z"
//     },
//     {
//         "id": 14,
//         "nombre": "Bella",
//         "especie": "Perro",
//         "tamano": "2",
//         "edad": 4,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T20:09:22.000000Z",
//         "updated_at": "2023-11-28T20:09:22.000000Z"
//     },
//     {
//         "id": 15,
//         "nombre": "Snowball",
//         "especie": "Gato",
//         "tamano": "3",
//         "edad": 2,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T22:17:35.000000Z",
//         "updated_at": "2023-11-28T22:17:35.000000Z"
//     },
//     {
//         "id": 16,
//         "nombre": "Lola",
//         "especie": "Conejo",
//         "tamano": "1",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-28T23:59:48.000000Z",
//         "updated_at": "2023-11-28T23:59:48.000000Z"
//     },
//     {
//         "id": 17,
//         "nombre": "Shadow",
//         "especie": "Perro",
//         "tamano": "2",
//         "edad": 5,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-29T02:10:05.000000Z",
//         "updated_at": "2023-11-29T02:10:05.000000Z"
//     },
//     {
//         "id": 18,
//         "nombre": "Oreo",
//         "especie": "Gato",
//         "tamano": "1",
//         "edad": 2,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-29T04:30:12.000000Z",
//         "updated_at": "2023-11-29T04:30:12.000000Z"
//     },
//     {
//         "id": 19,
//         "nombre": "Cotton",
//         "especie": "Conejo",
//         "tamano": "2",
//         "edad": 4,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-29T06:45:28.000000Z",
//         "updated_at": "2023-11-29T06:45:28.000000Z"
//     },
//     {
//         "id": 20,
//         "nombre": "Max",
//         "especie": "Perro",
//         "tamano": "3",
//         "edad": 3,
//         "descripcion": "Descripción del animal",
//         "created_at": "2023-11-29T08:55:42.000000Z",
//         "updated_at": "2023-11-29T08:55:42.000000Z"
//     }
// ];


const UTable = ({ data, columns, initialValues, inputs, onDel, onEdit }) => {


    const handleNextPage = () => {
        if (table.getCanNextPage()) {

            table.nextPage();
        }
    };

    const handleEdit = (data) =>{
        onEdit(data);
    }

    const handlePreviousPage = () => {
        if (table.getCanPreviousPage()){
            table.previousPage();

        }
    };

    // const columns = [
    //     { "header": "Id", "accessorKey": "id" },
    //     { "header": "Nombre", "accessorKey": "nombre" },
    //     { "header": "Especie", "accessorKey": "especie" },
    //     { "header": "Tamaño", "accessorKey": "tamano" },
    //     { "header": "Edad", "accessorKey": "edad" },
    //     { "header": "Descripción", "accessorKey": "descripcion" },
    //     { "header": "Creado en", "accessorKey": "created_at" },
    //     { "header": "Actualizado en", "accessorKey": "updated_at" },
    // ];

    const [filtering, setFiltering] = useState("");

    const table = useReactTable({
        data,
        columns,
        getCoreRowModel: getCoreRowModel(),
        getPaginationRowModel: getPaginationRowModel(),
        getFilteredRowModel: getFilteredRowModel(),
        state: {
            globalFilter: filtering
        },
        onGlobalFilterChange: setFiltering
    })

    const totalPages = table.getPageCount() || 1; // Asegúrate de manejar el caso en que totalPages sea null o undefined
    const [tooltipActive, setTooltipActive] = useState(false);
    const [tooltipActiveMap, setTooltipActiveMap] = useState({});
    console.log(totalPages)
    const getPageButton = (pageNumber) => (
        <button
            key={pageNumber}
            className={`relative h-8 max-h-[32px] w-8 max-w-[32px] select-none rounded-lg border border-gray-900 text-gray-900
                align-middle font-sans text-xs font-medium uppercase transition-all hover:bg-gray-900/10 active:bg-gray-900/20 disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none`}
            type="button"
            onClick={() => table.setPageIndex(pageNumber - 1)}

        >
            <span className="absolute transform -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2">
                {pageNumber}
            </span>
        </button>
    );

    const paginationButtons = [];

    if (totalPages <= 10) {
        // Muestra todos los botones si hay 10 o menos páginas
        for (let i = 1; i <= totalPages; i++) {
            paginationButtons.push(getPageButton(i));
        }
    } else {
        // Muestra los primeros 3, los últimos 3 y los puntos suspensivos
        for (let i = 1; i <= 3; i++) {
            paginationButtons.push(getPageButton(i));
        }

        paginationButtons.push(
            <button
                key="ellipsis-start"
                className="relative h-8 max-h-[32px] w-8 max-w-[32px] select-none text-center align-middle font-sans text-xs font-medium uppercase text-gray-900 transition-all"
                type="button"
                disabled
            >
                <span className="absolute transform -translate-x-1/2 -translate-y-1/2 top-1/2 left-1/2">...</span>
            </button>
        );

        // Muestra las últimas 3 páginas
        for (let i = totalPages - 2; i <= totalPages; i++) {
            paginationButtons.push(getPageButton(i));
        }
    }

    const handleClick = (row)=>{
        console.log("AAAAAA", row)
        onDel(row.original.id)
    }
    return (
        <div className="relative flex flex-col w-full h-full text-gray-700 bg-gray-200 shadow-md rounded-xl bg-clip-border">
            <div className="relative mx-4 mt-4 overflow-hidden text-gray-700 bg-opacity-10 rounded-none bg-clip-border">
                <div className="flex flex-col justify-between gap-8 mb-4 md:flex-row md:items-center">
                    <div>
                        <h5
                            className="block font-sans text-xl antialiased font-semibold leading-snug tracking-normal text-blue-gray-900">
                            Recent Transactions
                        </h5>
                        <p className="block mt-1 font-sans text-base antialiased font-normal leading-relaxed text-gray-700">
                            These are details about the last transactions
                        </p>
                    </div>
                    <div className="flex w-full gap-2 shrink-0 md:w-max">
                        <div className="w-full md:w-72">
                            <div className="relative h-10 w-full min-w-[200px]">
                                <div
                                    className="absolute grid w-5 h-5 top-2/4 right-3 -translate-y-2/4 place-items-center text-blue-gray-500">
                                    <i className="fas fa-search" aria-hidden="true"></i>
                                </div>
                                <Input
                                    label="Buscar"
                                    className="bg-transparent"
                                    onChange={(e) => setFiltering(e.target.value)}
                                />

                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div className="p-6 px-0">
                <table className="w-full text-left table-auto">
                    <thead>
                        {table.getHeaderGroups().map((headerGroup) => (
                            <tr key={headerGroup.id}>
                                <>
                                    {headerGroup.headers.map((header) => (
                                        <th className="p-4 border-y border-blue-gray-400 bg-blue-gray-100 p-4" key={header.id}>
                                            <Typography>
                                                {header.column.columnDef.header}
                                            </Typography>

                                        </th>
                                    ))}
                                    <th className="p-4 border-y border-blue-gray-400 bg-blue-gray-100 p-4">
                                        <Typography>
                                            Acciones
                                        </Typography>

                                    </th>
                                </>
                            </tr>
                        ))}
                    </thead>
                    <tbody className="w-full">
                        {table.getRowModel().rows.map((row, index) => (
                            <tr key={row.id} className="hover:bg-blue-200 hover:bg-opacity-20">
                                <>
                                    {row.getVisibleCells().map((cell) => {
                                        console.log(cell.column.columnDef.header != "Imagen")
                                        console.log(cell.getContext().getValue())
                                        return (
                                            <>
                                                <td key={cell.id} className="p-4 border-b bg-blue-gray-50/50 max-w-xs">
                                                    {cell.column.columnDef.header != "Imagen" ? <Typography>
                                                        {flexRender(cell.column.columnDef.cell, cell.getContext())}

                                                    </Typography> :
                                                        <div>
                                                            <Typography className="truncate ..."
                                                                onMouseEnter={() => setTooltipActiveMap({ ...tooltipActiveMap, [cell.id]: true })}
                                                                onMouseLeave={() => setTooltipActiveMap({ ...tooltipActiveMap, [cell.id]: false })}
                                                            >
                                                                {flexRender(cell.column.columnDef.cell, cell.getContext())}
                                                            </Typography>
                                                            {tooltipActiveMap[cell.id] && (
                                                                <div data-tooltip="tooltip-top-start" data-tooltip-placement="top-start"
                                                                    className="absolute z-50 whitespace-normal break-words rounded-lg bg-black font-sans text-sm font-normal text-white focus:outline-none">
                                                                    <div className="grid  w-full place-items-center overflow-x-scroll rounded-lg p-1 lg:overflow-visible">
                                                                        <img
                                                                            className="object-cover object-center w-full rounded-lg shadow-xl max-h-96 shadow-blue-gray-900/50"
                                                                            src={cell.getContext().getValue()}
                                                                            alt={cell.row.original.nombre}
                                                                        />
                                                                    </div>
                                                                </div>
                                                            )}
                                                        </div>}
                                                </td>

                                            </>


                                        )
                                    })}
                                    <td className="p-4 border-b bg-blue-gray-50/50 max-w-min">
                                        <div className="grid grid-flow-col auto-cols-max gap-x-1">
                                            {/* editar */}
                                            <div className="w-min" onClick={(e) => console.log(row)}>
                                                <SimpleDialog onEdit={true}>
                                                    <Dform initialValues={row.original} inputs={inputs} onSub={handleEdit}/>

                                                </SimpleDialog>
                                            </div>
                                            <div className="w-min">
                                                <IconButton className="bg-red-500" ripple={true} onClick={(e) => handleClick(row)}>
                                                    <i className="fas fa-trash" />
                                                </IconButton>
                                            </div>
                                        </div>
                                    </td>
                                </>
                            </tr>
                        ))}


                    </tbody>
                </table>
            </div>
            <div className="flex items-center justify-between p-4 border-t border-blue-gray-50">
                <Button variant="outlined" className="flex items-center gap-3" onClick={handlePreviousPage}>
                    <i className="fa-solid fa-left-long bg-gray"></i>
                    Anterior
                </Button>
                <div className="flex items-center gap-2">{paginationButtons}</div>

                <Button variant="outlined" className="flex items-center gap-3" onClick={handleNextPage}>
                    Siguiente
                    <i className="fa-solid fa-right-long bg-gray"></i>
                </Button>
                {/* <button
                    onClick={() => table.nextPage()}
                    className="select-none rounded-lg border border-gray-900 py-2 px-4 text-center align-middle font-sans text-xs font-bold uppercase text-gray-900 transition-all hover:opacity-75 focus:ring focus:ring-gray-300 active:opacity-[0.85] disabled:pointer-events-none disabled:opacity-50 disabled:shadow-none"
                    type="button">
                    Next
                </button> */}
            </div>
        </div>

    );
};

export default UTable;