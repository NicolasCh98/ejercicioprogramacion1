import { Button, Input, Typography } from "@material-tailwind/react";
import React from "react";
import DynamicSelect from "../dynamicSelect/dynamicSelect";

const DynamicFormd = ({ fields, onSubmit, initialValues = {} }) => {
    const [formData, setFormData] = React.useState(initialValues);

    const handleChange = (name, value) => {
        setFormData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleSubmit = () => {
        if (onSubmit) {
            console.log(formData);
            onSubmit(formData);
        }
    };

    return (
        <>
            {fields.map((field) => (
                <React.Fragment key={field.label}>
                    
                    {field.type === "select" ? (
                        <DynamicSelect
                            label={field.label}
                            options={field.options || []}
                            onChange={(selectedValue) => handleChange(field.label, selectedValue)}
                        />
                    ) : (
                        <Input
                            label={field.label}
                            size="lg"
                            type={field.type || "text"}
                            name={field.label}
                            value={formData[field.label] || ""}
                            onChange={(e) => handleChange(field.label, e.target.value)}
                        />
                    )}
                </React.Fragment>
            ))}
            <Button variant="gradient" onClick={handleSubmit} fullWidth>
                Submit
            </Button>
        </>
    );
};

export default DynamicFormd;