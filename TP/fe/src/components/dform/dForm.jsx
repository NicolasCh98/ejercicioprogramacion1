import { Button, Card, Input, Option, Select, Typography } from '@material-tailwind/react';
import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';


export default function Dform({ initialValues, inputs, title = "Formulario", buttonText = "Enviar", onSub }) {
    const { register, handleSubmit, formState: { errors }, setValue, getValues, watch, clearErrors, formState } = useForm({ defaultValues: initialValues });

    // const selectValue = initialValues.tamano
    console.log(initialValues)

    const onSubmit = handleSubmit((data) => {
        console.log(data);
        console.log(formState)
        onSub(data)
        if(typeof onClose === 'function'){
            onClose();

        }
    })

    const onChangeSe = (event) => {
        setValue("Select", event, true)
        console.log(event);
        console.log(getValues())

    }

    useEffect(() => {
      
    }, [formState.name])
    
    console.log(formState);
    return (
        <Card color="transparent" shadow={false}>
            <Typography variant="h4" color="blue-gray" className='mb-4'>
                {title}
            </Typography>
            <form onSubmit={onSubmit}>
                <div className="mb-1 flex flex-col gap-4">

                    {inputs.map((input, index) => (
                        <div key={input.name}>
                            <DynamicFormField watch={watch} initialValues={initialValues} setValue={setValue} clearErrors={clearErrors} onChangeSe={onChangeSe} name={input.name} key={index} register={register} validations={input.validations} errors={errors} {...input} options={input.options} />
                            {errors[input.name] && <Typography
                                variant="small"
                                color="red"
                                className="ml-2 mt-1 flex items-center gap-1 font-normal"

                            >{errors[input.name].message}</Typography>}
                        </div>

                    ))}

                    <div className="flex flex-col mx-auto">

                        <Button variant='gradient' color='indigo' type="submit" >
                            {buttonText}
                        </Button>

                    </div>

                </div>
            </form>
        </Card >
    );
}

const DynamicFormField = ({ type, name, validations, options, register, errors, onChangeSe, setValue, watch, clearErrors, initialValues }) => {
    switch (type) {
        case "text":
        case "date":
        case "number":
        case "password":
        case "email":
            return (
                <Input
                    color="blue"
                    label={name}
                    size="lg"
                    type={type}
                    name={name}
                    {...register(name, validations)}
                    error={errors[name] !== undefined && errors[name].message ? true : false}
                />

            );

        case "select":

            return (
                // <Select {...register("gender")} onChange={(e) => onChangeSe(e)}  >
                //         <Option value="female">female</Option>
                //         <Option value="male">male</Option>
                //         <Option value="other">other</Option>
                //     </Select>
                <DynamicSelectForm
                    label={name}
                    name={name}
                    options={options}
                    onChangese={onChangeSe}
                    register={register}
                    validations={validations}
                    errors={errors}
                    setValue={setValue}
                />

            );

        case "checkbox":
            return (
                <label className="flex items-center">
                    <input
                        type="checkbox"
                        {...register(name, validations)}
                        className="form-checkbox h-4 w-4 text-blue-500"
                    />
                    <span className="ml-2">{name}</span>
                </label>
            );

        // Agrega más casos según sea necesario para otros tipos de entrada
        case "file":

            function isURL(str) {
                // Utilizar una expresión regular simple para verificar si la cadena tiene un formato de URL
                return urlRegex.test(str);
            }
            const selectedFile = watch(name);

            const [imageSrc, setImageSrc] = useState(null);


            useEffect(() => {
                if (initialValues.id != null) {
                    if (selectedFile) {
                        const file = selectedFile[0];
                        console.log(selectedFile)
                        if (file) {

                            setImageSrc(selectedFile)

                            console.log(imageSrc)

                        }

                    }
                }

            }, [initialValues])


            useEffect(() => {
                if (initialValues.id != null) {
                    console.log("111111111111111111111")
                    if (selectedFile) {
                        const file = selectedFile[0];
                        console.log(selectedFile)
                        if (file) {
                            console.log("222222222222222222")
                            if (selectedFile instanceof FileList) {
                                const reader = new FileReader();

                                reader.onload = (e) => {
                                    setImageSrc(e.target.result);
                                    console.log(e.target.result)
    
                                };
                                reader.readAsDataURL(file);
                            }else{
                                setImageSrc(selectedFile)

                            }

                            console.log(imageSrc)

                        }

                    }
                } else {
                    if (selectedFile) {
                        console.log("33333333333333333")
                        const file = selectedFile[0];
                        console.log(selectedFile)
                        if (file) {
                            console.log("444444444444444444444")
                            const reader = new FileReader();

                            reader.onload = (e) => {
                                setImageSrc(e.target.result);
                                console.log(e.target.result)

                            };
                            reader.readAsDataURL(file);


                        }

                    }
                }



            }, [selectedFile])



            return (
                <div className="flex items-center justify-center w-full ">
                    <label htmlFor="dropzone-file" className="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-200 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                        <div className="flex flex-col items-center justify-center pt-5 pb-6">
                            {imageSrc ? (
                                <img
                                    className="h-40 w-full object-cover object-center rounded-lg object-cover object-center shadow-xl shadow-blue-gray-900/150"
                                    src={imageSrc}
                                    alt="Uploaded Image"

                                />
                            ) : (
                                <i className="fa-solid fa-cloud-arrow-up mb-4 text-gray-500 dark:text-gray-400"></i>
                            )}

                            <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                                <span className="font-semibold">Click to upload</span> or drag and drop
                            </p>
                            <p className="text-xs text-gray-500 dark:text-gray-400">
                                SVG, PNG, JPG or GIF (MAX. 800x400px)
                            </p>
                        </div>
                        <input
                            id="dropzone-file"
                            type="file"
                            className="hidden"
                            onChange={(e) => handleFileChange(e)}
                            {...register(name, validations)}
                        />
                    </label>
                </div>
            );
        default:
            return null;
    }
};

const DynamicSelectForm = ({ label, options, register, validations, errors, name, onChangese, setValue }) => {
    return (
        <div key={label}>
            <select className="customColor bg-white-50 border border-gray-400 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-2 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-blue-400 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" data-te-select-init data-te-select-size="lg" error={errors[name] !== undefined && errors[name].message ? true : false} name={name} label={label} size="lg" {...register(name, validations)}>
                <option value="" className="text-red-400" disabled selected hidden>Selecciona una opción</option>
                {options.map((option) => (
                    <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </select>
            {/* <Select value={"pequenho"} error={errors[name] !== undefined && errors[name].message ? true : false} name={name} label={label} size="lg" {...register(name, validations)} onChange={(e) => setValue(name, e, true)}  >
                {options.map((option) => (
                    <Option key={option.value} value={option.value}>
                        {option.label}
                    </Option>
                ))}
            </Select> */}
        </div>
    );
};