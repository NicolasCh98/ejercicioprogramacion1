import React, { useState } from "react";
import { Link, Outlet, useNavigate } from "react-router-dom";
import { Link as ScrollLink, animateScroll as scroll } from "react-scroll";
import logo from "../../../assets/team-padrinos-uca-logo.jpeg";
import { useAuth } from "../../../context/authProvider";
import loginService from "../../../services/loginService";
import DynamicDropdown from "../../dynamicDropdownMenu/DynamicDropdown";
// import Login from "../../login";










export default function NavBarLogged() {
  const { token, setToken, setCurrentUser } = useAuth();
  const navigate = useNavigate();

  function deleteCookie() {
    const now = new Date();
    const expirationDate = new Date(now.getTime() - (60 * 1000));
    // Ajusta la fecha a la zona horaria que desees
    console.log("dcookie");
    const expirationDateString = expirationDate.toUTCString();
    document.cookie = `token=; expires=${expirationDateString};`;
    // document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;`;
  }

  const handleLogoutClick = async () => {
    try {
      // Llama al servicio de logout
      await loginService.Logout(token);
  
      // Elimina la cookie
      deleteCookie();
  
      // Limpia el token en el contexto
      setToken("");
  
      // Limpia cualquier otro estado que desees restablecer en el contexto
      setCurrentUser(null);
  
      // Redirige a la página de inicio o a donde desees después del logout
      navigate('/');
    } catch (error) {
      console.error("Error durante el logout:", error);
      // Maneja el error según sea necesario
    }
  };
  // const scrollToConocenos = () => {
  //   // Si estamos en otra página que no sea la principal, navega a la principal y realiza el desplazamiento
  //   if (window.location.pathname !== "/") {
  //     navigate("/", {state: {scrollToConocenos: true } });
  //   } else {
  //     // Si ya estamos en la página principal, realiza solo el desplazamiento
  //     scroll.scrollTo(document.getElementById('conocenos').offsetTop, {
  //       duration: 500,
  //       smooth: 'easeInOutQuad',
  //     });
  //   }
  // };


  const [login, setLogin] = useState(false);
  // const toggleLogin = () => {
  //   setLogin(!login);

  // };

  return (
    <>
      <nav className="flex flex-wrap items-center justify-between px-2 py-3 bg-[#223b58]">
        <div className="container px-4 mx-auto flex items-center justify-between">
          <div className="w-full relative flex justify-between lg:w-auto lg:static lg:block lg:justify-start">
            <Link
              className="text-sm font-bold leading-relaxed inline-block mr-4 py-2 whitespace-nowrap uppercase text-white"
              to="/">
              <img className="w-[65px] h-auto rounded-full" src={logo} />
            </Link>
          </div>
          <div>
            <ul className="flex flex-row gap-10">
              {/* <Dropdown /> */}
              <li className="nav-item text-white">
                <DynamicDropdown
                  buttonText="Gestion de animales"
                  links={[
                    { to: '/home/rescates', label: 'Rescates' },
                    { to: '/home/solicitudes', label: 'Solicitudes' },
                    { to: '/home/animales', label: 'Animales' },
                    { to: '/home/adoptantes', label: 'Adoptantes' },
                  ]}
                />
              </li>
              {/* <li className="nav-item">
                <Link
                  className="px-3 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out"
                  to="/animales">
                  <span className="">Gestion de animales</span>
                </Link>
              </li> */}
              <li className="nav-item">
                <Link
                  className="px-3 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out"
                  to="/home/eventos">
                  <span className="">Gestion de eventos</span>
                </Link>
              </li>
              <li className="nav-item">
                <button
                  className="px-5 py-2 flex items-center text-sm uppercase font-bold leading-snug text-white hover:opacity-75 transition duration-300 ease-in-out bg-[#2ea8db] rounded-2xl"
                  onClick={handleLogoutClick}>
                  <span className="">Logout</span>
                </button>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <div className="bg-all">
        <Outlet />
      </div>
    </>
  );
}
