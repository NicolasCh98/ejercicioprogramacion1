import React from "react";
import whatsappLogo from "../../../assets/whatsapp-icon.png";
import facebookLogo from "../../../assets/facebook-icon.png";
import tiktokLogo from "../../../assets/tiktok-icon.png";
import instagramLogo from "../../../assets/instagram-icon.png";

export default function Footer(){
    return (
        <div className="flex flex-wrap items-center justify-between px-2 py-3 bg-[#223b58]">
            <div className="container px-4 mx-auto items-center justify-center">
                <div className="flex flex-row gap-10 justify-center">
                    <a target="_blank"  href="https://wa.me/+595981755022">
                        <img className="w-8" src={whatsappLogo} />
                    </a>
                    <a target="_blank" href="https://www.facebook.com/TeamPadrinosUCA?mibextid=LQQJ4d">
                        <img className="w-8" src={facebookLogo} />
                    </a>
                    <a target="_blank" href="https://www.tiktok.com/@teampadrinosuca?is_from_webapp=1&sender_device=pc">
                        <img className="w-8" src={tiktokLogo} />
                    </a>
                    <a target="_blank" href="https://www.facebook.com/TeamPadrinosUCA?mibextid=LQQJ4d">
                        <img className="w-8" src={instagramLogo} />
                    </a>
                </div>
            </div>
        </div>
    )
}