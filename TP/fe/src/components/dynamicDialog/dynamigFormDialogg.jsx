import React, { useEffect, useState } from "react";
import DynamicFormd from "../dynamicFormD/dynamicFormD";
import { Button, Card, CardBody, Dialog } from "@material-tailwind/react";


export function DynamicFormDialogg({
    fields,
    onSubmit,
    titulo,
    initialValues = {},
    row = {},
}) {
    const [open, setOpen] = useState(false);
    const [formData, setFormData] = useState(initialValues);

    const handleOpen = () => setOpen((cur) => !cur);

    useEffect(() => {
        console.log("hola")
        console.log(row)
        if (row.id != null) {
            const fechaOriginal = row.fecha_rescate;
            const fechaObjeto = new Date(fechaOriginal);
            const año = fechaObjeto.getFullYear();
            const mes = (fechaObjeto.getMonth() + 1).toString().padStart(2, '0');
            const dia = fechaObjeto.getDate().toString().padStart(2, '0');
            const fechaFormateada = `${año}-${mes}-${dia}`;

            setFormData({
                Nombre: row.animal.nombre || '',  // Asegúrate de manejar casos en los que animal no esté presente
                Especie: row.animal.especie || '',
                Tamaño: 1, // Cambia esto según tus necesidades
                Edad: row.animal.edad || '',
                Fecha_Rescate: fechaFormateada,
                Informacion_Adicional: row.informacion_adicional || '',
            });
            console.log(formData)
        }
    }, [open]);  // Asegúrate de incluir 'open' en las dependencias si es necesario

    const handleChange = (name, value) => {
        setFormData((prevData) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleSubmit = () => {
        if (onSubmit) {
            console.log(formData);
            onSubmit(formData);
        }
        handleOpen();  // Cierra el diálogo después de enviar el formulario
    };

    return (
        <>
            <div className="w-40">
                <Button onClick={handleOpen}>{titulo}</Button>
            </div>
            <Dialog size="xs" open={open} handler={handleOpen} className="bg-transparent shadow-none">
                <Card className="mx-auto w-full max-w-[24rem]">
                    <CardBody className="flex flex-col gap-4">
                        <DynamicFormd fields={fields} onSubmit={onSubmit} initialValues={formData} />
                    </CardBody>
                </Card>
            </Dialog>
        </>
    );
};