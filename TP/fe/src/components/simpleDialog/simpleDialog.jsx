import React, { useState } from "react";
import { Button, Card, CardBody, Dialog, DialogBody, DialogHeader, IconButton, Typography } from "@material-tailwind/react";

const SimpleDialog = ({ buttonText = "boton", dialogTitle = "", children, onEdit = false }) => {
    const [open, setOpen] = useState(false);

    const handleOpen = () => {
        setOpen(!open);
    };

    const handleClose = () => {
        console.log("hola")
        setOpen(false);
    };

    const formWithClose = React.cloneElement(children, { onClose: handleClose });

    return (
        <div>
            {onEdit ? <IconButton className="bg-blue-gray-900" ripple={true} onClick={handleOpen}>
                <i className="fas fa-edit" />
            </IconButton> : <Button variant="gradient" color="blue" onClick={handleOpen}>
                {buttonText}
            </Button>}


            <Dialog open={open} size="xs" handler={handleOpen} className="bg-transparent shadow-none">
                <Card className="mx-auto w-full">
                    <CardBody className="flex flex-col gap-4">
                        {/* <div className="flex items-center justify-between"> */}
                        {/* <DialogHeader className="flex flex-col items-start">
                            {" "} */}
                        <Typography className="mb-2" variant="h4" color="blue-gray">
                            {dialogTitle}
                        </Typography>
                        {/* </DialogHeader> */}

                        {/* </div> */}
                        <DialogBody>
                            {formWithClose}
                        </DialogBody>
                    </CardBody>
                </Card>
            </Dialog>
        </div>
    );
};
export default SimpleDialog;