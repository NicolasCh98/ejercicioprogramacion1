import { Button, Card, Input, Option, Select, Typography } from '@material-tailwind/react';
import React, { useState, useEffect, forwardRef, useImperativeHandle } from 'react';
import { useForm } from 'react-hook-form';


const FormSinSubmit = forwardRef(({ initialValues, inputs, buttonText = "Enviar", title = "Formulario", onClose, onSub, mostrarSubmit = true }, ref) => {

    const { register, handleSubmit, formState: { errors }, setValue, getValues, watch, clearErrors, formState } = useForm({ defaultValues: initialValues });
    const [isValid, setIsValid] = useState(formState.isValid)

    // const selectValue = initialValues.tamano
    console.log(initialValues)

    const getFormData = () => {
        const formData = getValues();
        // Realiza cualquier procesamiento adicional si es necesario
        return formData;
    };

    useImperativeHandle(ref, () => ({
        getFormData,
        onSubmit: onSubmit,
        isValid: formState.isValid,
        onSubmitPrev: onSubmitPrev,
    }));

    // const onSubmitPrev = () => {
    //     // const hasErrors = formState.isValid;
    //     console.log("ERROREEES", formState.isValid)
    //     setIsValid(formState.isValid);

    // };

    const onSubmitPrev = async () => {
        // Realiza alguna lógica previa si es necesario

        // Espera a que onSubmitPrev complete su ejecución
        // await someAsyncFunction();

        // Ahora puedes acceder a formState.isValid
        console.log("ERROREEES", formState.isValid);

        // Actualiza el estado isValid o realiza otras acciones si es necesario
        setIsValid(formState.isValid);
    };

    const onSubmit = handleSubmit((data) => {
        console.log(data);
        console.log("AAAAAAAAAAAAAAAAAAAAAAAAA")
        onSub(data)
        if (typeof onClose === 'function') {
            onClose();

        }
    })

    const onChangeSe = (event) => {
        setValue("Select", event, true)
        console.log(event);
        console.log(getValues())

    }

    // useEffect(() => {

    // }, [initialValues])

    console.log(formState);
    return (
        <Card color="transparent" shadow={false}>
            <Typography variant="h4" color="blue-gray" className='mb-4'>
                {title}
            </Typography>
            <form onSubmit={onSubmit} ref={ref}>
                <div className="mb-1 flex flex-col gap-1">

                    {inputs.map((input, index) => (
                        <div key={input.name}>
                            <DynamicFormField watch={watch} initialValues={initialValues} setValue={setValue} clearErrors={clearErrors} onChangeSe={onChangeSe} name={input.name} key={index} register={register} validations={input.validations} errors={errors} {...input} options={input.options} />
                            <div className='h-[21px] mt-1'>
                                {errors[input.name] && <Typography
                                    variant="small"
                                    color="red"
                                    className="ml-2 flex items-center gap-1 font-normal"

                                >{errors[input.name].message}</Typography>}

                            </div>
                        </div>

                    ))}

                    <div className="flex flex-col mx-auto">

                        {/* <Button variant='gradient' color='indigo' type="submit" >
                                {buttonText}
                            </Button>
                         */}

                    </div>

                </div>
            </form>
        </Card >
    );
})
export default FormSinSubmit;

const DynamicFormField = ({ type, name, validations, options, register, errors, onChangeSe, setValue, watch, clearErrors, initialValues }) => {
    switch (type) {
        case "text":
        case "date":
        case "number":
        case "password":
        case "email":
            return (
                <Input
                    color="blue"
                    label={name}
                    size="lg"
                    type={type}
                    name={name}
                    {...register(name, validations)}
                    error={errors[name] !== undefined && errors[name].message ? true : false}
                />

            );

        case "select":

            return (

                <DynamicSelectForm
                    label={name}
                    name={name}
                    options={options}
                    onChangese={onChangeSe}
                    register={register}
                    validations={validations}
                    errors={errors}
                    setValue={setValue}
                />

            );

        case "checkbox":
            return (
                <label className="flex items-center">
                    <input
                        type="checkbox"
                        {...register(name, validations)}
                        className="form-checkbox h-4 w-4 text-blue-500"
                    />
                    <span className="ml-2">{name}</span>
                </label>
            );

        // Agrega más casos según sea necesario para otros tipos de entrada
        case "file":

            function isURL(str) {
                // Utilizar una expresión regular simple para verificar si la cadena tiene un formato de URL
                const urlRegex = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/;
                return urlRegex.test(str);
            }
            const selectedFile = watch(name);

            const [imageSrc, setImageSrc] = useState(null);


            useEffect(() => {
                if (initialValues.id != null) {
                    if (selectedFile) {
                        const file = selectedFile[0];
                        console.log(selectedFile)
                        if (file) {

                            setImageSrc(selectedFile)

                            console.log(imageSrc)

                        }

                    }
                }

            }, [initialValues])


            useEffect(() => {
                if (initialValues.id != null) {
                    if (selectedFile) {
                        const file = selectedFile[0];
                        console.log(selectedFile)
                        if (file) {

                            setImageSrc(selectedFile)

                            console.log(imageSrc)

                        }

                    }
                } else {
                    if (selectedFile) {
                        const file = selectedFile[0];
                        console.log(selectedFile)
                        if (file) {
                            const reader = new FileReader();

                            reader.onload = (e) => {
                                setImageSrc(e.target.result);
                                console.log(e.target.result)

                            };
                            reader.readAsDataURL(file);


                        }

                    }
                }



            }, [selectedFile])



            return (
                <div className="flex items-center justify-center w-full ">
                    <label htmlFor="dropzone-file" className="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-200 dark:hover:bg-bray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                        <div className="flex flex-col items-center justify-center pt-5 pb-6">
                            {imageSrc ? (
                                <img
                                    className="h-40 w-full object-cover object-center rounded-lg object-cover object-center shadow-xl shadow-blue-gray-900/150"
                                    src={imageSrc}
                                    alt="Uploaded Image"

                                />
                            ) : (
                                <i className="fa-solid fa-cloud-arrow-up mb-4 text-gray-500 dark:text-gray-400"></i>
                            )}

                            <p className="mb-2 text-sm text-gray-500 dark:text-gray-400">
                                <span className="font-semibold">Click to upload</span> or drag and drop
                            </p>
                            <p className="text-xs text-gray-500 dark:text-gray-400">
                                SVG, PNG, JPG or GIF (MAX. 800x400px)
                            </p>
                        </div>
                        <input
                            id="dropzone-file"
                            type="file"
                            className="hidden"
                            onChange={(e) => handleFileChange(e)}
                            {...register(name, validations)}
                        />
                    </label>
                </div>
            );
        default:
            return null;
    }
};

const DynamicSelectForm = ({ label, options, register, validations, errors, name, onChangese, setValue }) => {
    return (
        <div key={label}>
            <select
                defaultValue=""  // Establece el valor predeterminado aquí
                className="customColor bg-white-50 border border-gray-400 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-2 focus:border-blue-500 block w-full p-2.5 dark:bg-white-700 dark:border-blue-400 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                data-te-select-init
                data-te-select-size="lg"
                name={name}
                label={label}
                size="lg"
                {...register(name, validations)}
            >
                <option value="" disabled hidden>
                    Selecciona una opción
                </option>
                {options.map((option) => (
                    <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </select>
            {/* <Select value={"pequenho"} error={errors[name] !== undefined && errors[name].message ? true : false} name={name} label={label} size="lg" {...register(name, validations)} onChange={(e) => setValue(name, e, true)}  >
                {options.map((option) => (
                    <Option key={option.value} value={option.value}>
                        {option.label}
                    </Option>
                ))}
            </Select> */}
        </div>
    );
};