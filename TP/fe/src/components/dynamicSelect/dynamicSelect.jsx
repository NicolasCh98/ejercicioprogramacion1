import { Option, Select } from "@material-tailwind/react";

const DynamicSelect = ({ label, onChange, options }) => {
    return (
        <div key={label}>
            <Select label={label} onChange={onChange} size="md">
                {options.map((option) => (
                    <Option key={option.id} value={String(option.id)}>
                        {option.text}
                    </Option>
                ))}
            </Select>
        </div>
    );
};

export default DynamicSelect;