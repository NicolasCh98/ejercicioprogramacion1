import { IconButton, Typography } from "@material-tailwind/react";
import { DynamicFormDialogg } from "../dynamicDialog/dynamigFormDialogg";

const DynamicTable = ({ data, formFields, onEdit, selectedColumns = [], handleDelete }) => {
    // Obtener todas las columnas de todos los objetos
    const columns = Array.from(new Set(data.flatMap((item) => Object.keys(item))));

    // Filtrar las columnas si se proporcionan columnas seleccionadas
    const visibleColumns = selectedColumns.length > 0 ? selectedColumns : columns;

    return (
        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
            <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
                <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        {visibleColumns.map((column, index) => (
                            <th key={index} className="px-6 py-3">
                                {String(column)}
                            </th>
                        ))}
                        <th scope="col" className="px-6 py-3">
                            <span className="sr-only">Edit</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {data.map((row, rowIndex) => (
                        <tr
                            key={rowIndex}
                            className={`${rowIndex % 2 === 0 ? "bg-white" : "bg-white dark:bg-gray-800"
                                } border-b dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600`}>
                            {selectedColumns.map((column, colIndex) => (
                                <td key={colIndex} className="px-6 py-4 whitespace-nowrap">
                                    <Typography variant="small" color="blue-gray" className="font-normal">
                                        {row.animal[column] || row[column]}
                                    </Typography>
                                </td>
                            ))}
                            <td className="px-6 py-4 text-right">
                                <div className="grid grid-cols-2">
                                    <div className="w-min">
                                        <DynamicFormDialogg
                                            fields={formFields}
                                            onSubmit={() => onEdit(row)}
                                            titulo={"Editar"}
                                            row={row}
                                        />
                                    </div>
                                    <div className="w-min">
                                        <IconButton className="bg-red-500" ripple={true} onClick={() => { handleDelete(row.id) }}>
                                            <i className="fas fa-trash" />
                                        </IconButton>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default DynamicTable;